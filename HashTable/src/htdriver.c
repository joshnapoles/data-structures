/**************************************************************************
 File name:  htdriver.c
 Author:     josh napoles
 Date:			 12.3.18
 Class:			 CS300
 Assignment: HashTable
 Purpose:    Interface for a Hash Table
 *************************************************************************/

#include "../include/ht.h"

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <ctype.h>


/**************************************************************************
 * Function:		success
 *
 * Description:	print a success message
 *
 * Parameters:	pszStr	-	the message to print
 *
 * Returned:		none
 *************************************************************************/
static void success (char *pszStr)
{
	printf ("SUCCESS: %s\n", pszStr);
}
/**************************************************************************
 * Function:		failure
 *
 * Description:	print a failure message
 *
 * Parameters:	pszStr	-	the message to print
 *
 * Returned:		none
 *************************************************************************/
static void failure (char *pszStr)
{
	printf ("FAILURE: %s\n", pszStr);
}
/**************************************************************************
 * Function:		assert
 *
 * Description:	if the expression is true, assert success; otherwise,
 * 							assert failure
 *
 * Parameters:	bExpression  - true or false expression to be asserted
 * 							pszTrue		   - the message to print when true
 * 							pszFalse	   - the message to print when false
 *
 * Returned:		none
 *************************************************************************/
static void assert (bool bExpression, char *pszTrue, char *pszFalse)
{
	if (bExpression)
	{
		success (pszTrue);
	}
	else if (!bExpression)
	{
		failure (pszFalse);
	}
}
/**************************************************************************
 * Function:		HashInt
 *
 * Description:	returns a bucket using midsquare
 *
 * Parameters:	pKey		- ptr to integer key to be hashed
 * 							htSize	- number of buckets in the hash table
 *
 * Returned:		int			- bucket that key hashes to
 *************************************************************************/
int HashInt (void *pKey, int htSize)
{
	unsigned int key = *(int*) pKey;
	unsigned int middle;
	int bucket;

	if (NULL == pKey)
	{
		fprintf (stderr, "ERROR: INVALID KEY\n");
		exit (0);
	}

	key *= key;
	middle = (key & 0x000ff000) >> 12;
	bucket = middle % htSize;

	return bucket;
}
/**************************************************************************
 * Function:		compareIntKey
 *
 * Description:	compares two integer keys
 *
 * Parameters:	pKey1	- integer key ptr
 * 							pKey2	- integer key ptr
 *
 * Returned:		true if keys are equivalent; false otherwise
 *************************************************************************/
bool compareIntKey (void *pKey1, void *pKey2)
{
	bool bSame = false;
	if (*(int*) pKey1 == *(int*)pKey2)
	{
		bSame = true;
	}

	return bSame;
}
/**************************************************************************
 * Function:		printIntElement
 *
 * Description:	prints a HashTableElement of form KEY:{int} DATA:{string}
 *
 * Parameters:	HashTableElementPtr	- ptr to HashTableElement
 *
 * Returned:		none
 *************************************************************************/
void printIntElement (HashTableElementPtr psElement)
{
		if (NULL == psElement)
		{
			fprintf (stderr, "ERROR: INVALID ELEMENT\n");
			exit (0);
		}

		printf ("K:%d D:%s", *(int*) psElement->pKey, (char*) psElement->pData);
}
/**************************************************************************
 * Function:		HashString
 *
 * Description:	hashes a string to a bucket in the HashTable
 *
 * Parameters:	pKey1	- integer key ptr
 * 							htSize- size of the HashTable
 *
 * Returned:		int		- bucket that maps to hashed string
 *************************************************************************/
int HashString (void *pKey, int htSize)
{
	int value = 0;
	char *h;
	for (int i = 0; i < MAX_STR_LEN; i++)
	{
		h = (char*) pKey;


		value = (*h) * 31 + value;
	}

	value %= htSize;

	return value;
}
/**************************************************************************
 * Function:		compareStringKey
 *
 * Description:	compares two string keys
 *
 * Parameters:	pKey1	- string key ptr
 * 							pKey2	- string key ptr
 *
 * Returned:		true if strings are equivalent; false otherwise
 *************************************************************************/
bool compareStringKey (void *pKey1 , void *pKey2)
{
	bool bSameKey = false;

	if (0 == strncmp ((char*)pKey1, (char*)pKey2, MAX_STR_LEN))
	{
		bSameKey = true;
	}

	return bSameKey;
}
/**************************************************************************
 * Function:		printStringElement
 *
 * Description:	prints a HashTableElement of form KEY:{string} DATA:{int}
 *
 * Parameters:	HashTableElementPtr	- ptr to HashTableElement
 *
 * Returned:		none
 *************************************************************************/
void printStringElement (HashTableElementPtr psElement)
{
	if (NULL == psElement)
	{
		fprintf (stderr, "ERROR: INVALID ELEMENT\n");
		exit (0);
	}

	printf ("K:%s D:%d", (char*) psElement->pKey, *(int*) psElement->pData);
}


/**************************************************************************
 * Function:		main
 *
 * Description:	tests the functionality of the priority queue
 *
 * Parameters: 	none
 *
 * Returned:		EXIT_SUCCESS
 *************************************************************************/
#define MAX_NAME 16
int main ()
{
	HashTable sHT, sHT2;
	bool bFound = false;
	int htSize = 5;
	int key = 150;
	char name[5][MAX_NAME];
	HashTableElement sHE;

	int i;

	memset (name[0], 0, MAX_NAME);
	strcpy (name[0], "Shawn");

	memset (name[1], 0, MAX_NAME);
	strcpy (name[1], "Jonah");

	memset (name[2], 0, MAX_NAME);
	strcpy (name[2], "Maddie");

	memset (name[3], 0, MAX_NAME);
	strcpy (name[3], "Larry");

	memset (name[4], 0, MAX_NAME);
	strcpy (name[4], "Ryan");



	puts ("PROGRAM START\n");

	printf ("Creating HashTable1 with %d buckets\n", htSize);
	htCreate (&sHT, htSize, &HashInt, &compareIntKey, &printIntElement);
	assert (htSize == getNumBuckets (&sHT),
			"Table size is 5", "Table size is not 5");

	assert (htIsEmpty (&sHT), "HashTable is Empty","HashTable is not Empty");
	assert (!htIsFull(&sHT), "HashTable is not Full","HashTable is Full");


	printf ("Inserting Element\n");
	htInsertElement (&sHT, &key, name[0], sizeof (int), MAX_STR_LEN);

	htSearch (&sHT, &key, sizeof (int), sizeof (char), &bFound);
	assert (bFound, "Element found in HashTable",
			"Element not found in HashTable");


	printf ("\nInserting 5 Elements into table\n");
	memset (name[0], 0, MAX_NAME);
	strcpy (name[0], "Josh");
	for (i = 0; i < 5; i++)
	{
		key += 147;
		htInsertElement(&sHT, &key, name[i], sizeof (int), MAX_NAME);
	}

	assert (!htIsEmpty (&sHT), "HashTable is not Empty","HashTable is Empty");
	assert (!htIsFull(&sHT), "HashTable is not Full","HashTable is Full");

	puts("");
	printHT(&sHT);
	puts ("");


	printf ("Searching table for Key: 297\n");
	key = 297;
	sHE = htSearch (&sHT, &key, sizeof (int), MAX_STR_LEN, &bFound);
	assert (bFound, "Key: 297 Found in HashTable",
			"Key: 297 Not Found in HashTable");
	printf ("Data retrieved by Key: 297  -> %s\n", (char*) sHE.pData);


	key = 150;
	printf ("Deleting data of key 150\n");
	htDeleteElement(&sHT, &key);
	htSearch (&sHT, &key, sizeof (int), MAX_STR_LEN, &bFound);
	assert (!bFound, "Key: 150 deleted from HashTable",
			"Key: 150 Found in HashTable");


	puts("");
	printHT(&sHT);
	puts ("");

	key = 297;
	printf ("Updating data of key 297\n");
	bFound = false;
	memset(name[0], 0, MAX_STR_LEN);
	strcpy (name[0], "Justin");
	bFound = htUpdateElement (&sHT, &key, name[0], MAX_STR_LEN);
	assert (bFound, "Updated key 297", "Did not update key 297");

	puts("");
	printHT(&sHT);
	puts ("");


	printf ("Terminating HashTable1\n");
	htTerminate (&sHT);

	htSize+= 2;
	key = 100;
	printf ("Creating HashTable2 with %d buckets\n", htSize);
	htCreate (&sHT2, htSize, &HashString, &compareStringKey,
			&printStringElement);
	assert (htSize == getNumBuckets (&sHT2),
			"Table size is 7", "Table size is not 7");

	printf ("\nInserting 5 Elements\n");
	for (i = 0; i < 5; i++)
	{
		key+=100;
		htInsertElement (&sHT2, name[i], &key, MAX_STR_LEN, sizeof (int));
	}

	puts("");
	printHT(&sHT2);
	puts ("");


	printf ("Updating Key %s\n", name[3]);
	key = 9999;
	bFound = htUpdateElement (&sHT2, name[3], &key, sizeof (int));
	assert (bFound, "Element Updated", "Element not Updated");


	puts("");
	printHT(&sHT2);
	puts ("");


	htTerminate (&sHT2);
	puts ("PROGRAM END");
	return EXIT_SUCCESS;
}
