/**************************************************************************
 File name:  ht.c
 Author:     josh napoles
 Date:			 12.3.18
 Class:			 CS300
 Assignment: HashTable
 Purpose:    Interface for a generic dynamic hashtable
 *************************************************************************/
#include "../include/ht.h"
#include <stdio.h>
#include <stdlib.h>

/**************************************************************************
 * Function:		 htCreate
 *
 * Description:	 If HT can be created, then HT exists and is empty
 *
 * Parameters:	 psHT		- pointer to HashTable
 * 							 htSize - size of HashTable
 * 							 pHashFunction	- pointer to Hash Function
 * 							 pCompFunction	- pointer to key compare Function
 * 							 pPirntFunction	- pointer to print Function
 *
 * Returned:		 none
 *************************************************************************/
void htCreate (HashTablePtr psHT, int htSize, Hash pHashFunction,
		compareKey pCompFunction, printHashElement pPrintFunction)
{
	int i;
	if (NULL == psHT)
	{
		fprintf (stderr, "ERROR: INVALID HASH TABLE\n");
		exit (0);
	}
	else if (NULL == pHashFunction)
	{
		fprintf (stderr, "ERROR: INVALID HASH FUNCTION\n");
		exit (0);
	}
	else if (NULL == pCompFunction)
	{
		fprintf (stderr, "ERROR: INVALID KEY COMPARE FUNCTION\n");
		exit (0);
	}
	else if (NULL == pPrintFunction)
	{
		fprintf (stderr, "ERROR: INVALID PRINT FUNCTION\n");
		exit (0);
	}

	psHT->numBuckets = htSize;
	psHT->pHashFunction = pHashFunction;
	psHT->pCompareFunction = pCompFunction;
	psHT->pPrintFunction = pPrintFunction;

	psHT->psLists = malloc (sizeof (List) * psHT->numBuckets);

	for (i = 0; i < psHT->numBuckets; i++)
	{
		lstCreate (&psHT->psLists[i]);
	}
}
/**************************************************************************
 * Function:		 htTerminate
 *
 * Description:	 free HashTable Memory
 *
 * Parameters:	 psHT		- pointer to HashTable
 *
 * Returned:		 none
 *************************************************************************/
void htTerminate (HashTablePtr psHT)
{
	int i;
	HashTableElement sTemp;
	if (NULL == psHT)
	{
		fprintf (stderr, "ERROR: INVALID HASH TABLE\n");
		exit (0);
	}

	for (i = 0; i < psHT->numBuckets; i++)
	{
		while(!lstIsEmpty(&psHT->psLists[i]))
		{
			lstDeleteCurrent(&psHT->psLists[i], &sTemp, sizeof (HashTableElement));
			free (sTemp.pData);
			free (sTemp.pKey);
		}
		lstTerminate (&psHT->psLists[i]);
	}

	free (psHT->psLists);
	psHT->pHashFunction = NULL;
	psHT->pCompareFunction = NULL;
	psHT->pPrintFunction = NULL;
	psHT = NULL;
}
/**************************************************************************
 * Function:		 getNumBuckets
 *
 * Description:	 returns the number of buckets in the hash table
 *
 * Parameters:	 psHT		- pointer to HashTable
 *
 * Returned:		 int 	- number of buckets
 *************************************************************************/
int getNumBuckets (HashTablePtr psHT)
{
	if (NULL == psHT)
	{
		fprintf (stderr, "ERROR: INVALID HASH TABLE\n");
		exit (0);
	}

	return psHT->numBuckets;
}
/**************************************************************************
 * Function:		 htIsEmpty
 *
 * Description:	 checks to see if the Hash Table is empty
 *
 * Parameters:	 psHT		- pointer to HashTable
 *
 * Returned:		 bool		-	returns true if no elements are in the HashTable
 *************************************************************************/
bool htIsEmpty (HashTablePtr psHT)
{
	bool bHTIsEmpty = true;
	int i;
	if (NULL == psHT)
	{
		fprintf (stderr, "ERROR: INVALID HASH TABLE\n");
		exit (0);
	}

	//check all lists for elements
	for (i = 0; i < psHT->numBuckets; i++)
	{
		if (!lstIsEmpty(&psHT->psLists[i]))
		{
			bHTIsEmpty = false;
		}
	}

	return bHTIsEmpty;
}
/**************************************************************************
 * Function:		 htIsFull
 *
 * Description:	 checks to see if the hash table is full
 *
 * Parameters:	 psHT		- pointer to HashTable
 *
 * Returned:		 bool		- true if any bucket in hash table is full
 *************************************************************************/
bool htIsFull (HashTablePtr psHT)
{
	bool bHTIsFull = false;
	int i;
	if (NULL == psHT)
	{
		fprintf (stderr, "ERROR: INVALID HASH TABLE\n");
		exit (0);
	}

	//check all lists
	for (i = 0; i < psHT->numBuckets; i++)
	{
		if (lstIsFull (&psHT->psLists[i]))
		{
			bHTIsFull = true;
		}
	}

	return bHTIsFull;
}
/**************************************************************************
 * Function:		 htInsertElement
 *
 * Description:	 inserts an key and data into hash table if key is not
 * 							 already in table
 *
 * Parameters:	 psHT		- pointer to HashTable
 * 							 pKey		- Hash Key
 * 							 pData	- data to be inserted
 * 							 keySize	- size of key
 * 							 dataSize	- size of data
 *
 * Returned:		 none
 *************************************************************************/
void htInsertElement (HashTablePtr psHT, void *pKey,
		void *pData, int keySize, int dataSize)
{
	HashTableElement sHE;
	int bucket;
	bool bFound = false;
	if (NULL == psHT)
	{
		fprintf (stderr, "ERROR: INVALID HASH TABLE\n");
		exit (0);
	}
	else if (NULL == pKey)
	{
		fprintf (stderr, "ERROR: INVALID KEY\n");
		exit (0);
	}
	else if (NULL == pData)
	{
		fprintf (stderr, "ERROR: INVALID DATA\n");
		exit (0);
	}
	else if (NULL == psHT->pHashFunction)
	{
		fprintf (stderr, "ERROR: INVALID HASH FUNCTION\n");
		exit (0);
	}
	else if (NULL == psHT->pCompareFunction)
	{
		fprintf (stderr, "ERROR: INVALID KEY COMPARE FUNCTION\n");
		exit (0);
	}

	// if key is in table do not insert
	htSearch (psHT, pKey, keySize, dataSize, &bFound);

	if (!bFound && !htIsFull(psHT))
	{
		sHE.pKey = malloc (keySize);
		sHE.pData = malloc (dataSize);
		memcpy (sHE.pKey, pKey, keySize);
		memcpy (sHE.pData, pData, dataSize);

		bucket = psHT->pHashFunction (pKey, psHT->numBuckets);
		//fprintf (stderr, "Hashed to bucket# %d \n", bucket);

		if (!lstIsEmpty(&psHT->psLists[bucket]))
		{lstLast (&psHT->psLists[bucket]);}

		lstInsertAfter (&psHT->psLists[bucket], &sHE, sizeof (HashTableElement));
	}

}
/**************************************************************************
 * Function:		 htSearch
 *
 * Description:	 searches for the key in the hash table
 *
 * Parameters:	 psHT			- pointer to HashTable
 * 							 pKey			- Hash Key
 * 							 pData		- data to be inserted
 * 							 keySize	- size of key
 * 							 dataSize	- size of data
 * 							 bFound		- true if key was found in table; false otherwise
 *
 * Returned:		 HashTableElement	-	Element that maps to the key
 *************************************************************************/
HashTableElement htSearch (HashTablePtr psHT, void *pKey, int pKeySize,
		int dataSize, bool *bFound)
{
	HashTableElement sHE;
	int bucket;
	*bFound = false;
	if (NULL == psHT)
	{
		fprintf (stderr, "ERROR: INVALID HASH TABLE\n");
		exit (0);
	}
	else if (NULL == psHT->pHashFunction)
	{
		fprintf (stderr, "ERROR: INVALID HASH FUNCTION\n");
		exit (0);
	}
	else if (NULL == psHT->pCompareFunction)
	{
		fprintf (stderr, "ERROR: INVALID KEY COMPARE FUNCTION\n");
		exit (0);
	}

	bucket = psHT->pHashFunction (pKey, psHT->numBuckets);

	if (!lstIsEmpty (&psHT->psLists[bucket]))
	{
		lstFirst (&psHT->psLists[bucket]);
		lstPeek (&psHT->psLists[bucket], &sHE, sizeof (HashTableElement));
		*bFound = psHT->pCompareFunction (pKey, sHE.pKey);
		while (lstHasNext (&psHT->psLists[bucket]) && !(*bFound))
		{
			lstNext (&psHT->psLists[bucket]);
			lstPeek (&psHT->psLists[bucket], &sHE, sizeof (HashTableElement));
			*bFound = psHT->pCompareFunction (pKey, sHE.pKey);
		}
	}


	return sHE;
}
/**************************************************************************
 * Function:		 printHT
 *
 * Description:	 print the hash table
 *
 * Parameters:	 psHT		- pointer to HashTable
 *
 * Returned:		 none
 *************************************************************************/
void printHT (HashTablePtr psHT)
{
	int i;
	HashTableElement sHE;
	if (NULL == psHT)
	{
		fprintf (stderr, "ERROR: INVALID HASH TABLE\n");
		exit (0);
	}
	else if (NULL == psHT->pPrintFunction)
	{
		fprintf (stderr, "ERROR: INVALID PRINT FUNCTION\n");
		exit (0);
	}

	puts("-----------------------------------------------------------");
	printf ("Bucket\n");
	for (i = 0; i < psHT->numBuckets; i++)
	{
		printf ("%3d", i);
		if (!lstIsEmpty (&psHT->psLists[i]))
		{
			printf ("  ");
			lstFirst (&psHT->psLists[i]);
			lstPeek (&psHT->psLists[i], &sHE, sizeof (HashTableElement));
			psHT->pPrintFunction (&sHE);

			while (lstHasNext (&psHT->psLists[i]))
			{
				printf (" -> ");
				lstNext (&psHT->psLists[i]);
				lstPeek (&psHT->psLists[i], &sHE, sizeof (HashTableElement));
				psHT->pPrintFunction (&sHE);
			}

		}
		printf ("-|\n");
	}

	puts("-----------------------------------------------------------");
}
/**************************************************************************
 * Function:		 htDeleteElement
 *
 * Description:	 Deletes an element of the key that maps to the pKey
 *
 * Parameters:	 psHT		- pointer to HashTable
 * 							 pKey		- Hash Key
 *
 * Returned:		 bool 	-	true if the element was found and deleted
 *************************************************************************/
bool htDeleteElement (HashTablePtr psHT, void *pKey)
{
	HashTableElement sHE;
	int bucket;
	bool bFound = false;

	if (NULL == psHT)
	{
		fprintf (stderr, "ERROR: INVALID HASH TABLE\n");
		exit (0);
	}
	else if (NULL == psHT->pHashFunction)
	{
		fprintf (stderr, "ERROR: INVALID HASH FUNCTION\n");
		exit (0);
	}
	else if (NULL == psHT->pCompareFunction)
	{
		fprintf (stderr, "ERROR: INVALID KEY COMPARE FUNCTION\n");
		exit (0);
	}

	bucket = psHT->pHashFunction (pKey, psHT->numBuckets);
	if (!lstIsEmpty (&psHT->psLists[bucket]))
	{
		lstFirst (&psHT->psLists[bucket]);
		lstPeek (&psHT->psLists[bucket], &sHE, sizeof (HashTableElement));
		bFound = psHT->pCompareFunction (pKey, sHE.pKey);
		while (lstHasNext (&psHT->psLists[bucket]) && !bFound)
		{
			lstNext (&psHT->psLists[bucket]);
			lstPeek (&psHT->psLists[bucket], &sHE, sizeof (HashTableElement));
			bFound = psHT->pCompareFunction (pKey, sHE.pKey);
		}

		if (bFound)
		{
			lstDeleteCurrent (&psHT->psLists[bucket], &sHE,
					sizeof (HashTableElement));
			free (sHE.pData);
			free (sHE.pKey);
		}
	}

	return bFound;
}
/**************************************************************************
 * Function:		 htUpdateElement
 *
 * Description:	 updates the data of the passed in pKey
 *
 * Parameters:	 psHT		- pointer to HashTable
 * 							 pKey		- Hash Key
 * 							 pData	- data to be inserted
 * 							 dataSize	- size of data
 *
 * Returned:		 bool	- true if the element was found and updated
 *************************************************************************/
bool htUpdateElement (HashTablePtr psHT, void *pKey, void *pData,
		int dataSize)
{
	HashTableElement sHE;
	int bucket;
	bool bFound = false;

	if (NULL == psHT)
	{
		fprintf (stderr, "ERROR: INVALID HASH TABLE\n");
		exit (0);
	}
	else if (NULL == psHT->pHashFunction)
	{
		fprintf (stderr, "ERROR: INVALID HASH FUNCTION\n");
		exit (0);
	}
	else if (NULL == psHT->pCompareFunction)
	{
		fprintf (stderr, "ERROR: INVALID KEY COMPARE FUNCTION\n");
		exit (0);
	}

	bucket = psHT->pHashFunction (pKey, psHT->numBuckets);
	if (!lstIsEmpty (&psHT->psLists[bucket]))
	{
		lstFirst (&psHT->psLists[bucket]);
		lstPeek (&psHT->psLists[bucket], &sHE, sizeof (HashTableElement));
		bFound = psHT->pCompareFunction (pKey, sHE.pKey);

		while (lstHasNext (&psHT->psLists[bucket]) && !bFound)
		{
			lstNext (&psHT->psLists[bucket]);
			lstPeek (&psHT->psLists[bucket], &sHE, sizeof (HashTableElement));
			bFound = psHT->pCompareFunction (pKey, sHE.pKey);
		}

		if (bFound)
		{
			memcpy (sHE.pData, pData, dataSize);
			lstUpdateCurrent (&psHT->psLists[bucket], &sHE, sizeof (HashTableElement));
		}
	}

	return bFound;
}



