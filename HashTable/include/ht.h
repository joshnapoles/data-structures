/**************************************************************************
 File name:		  ht.h
 Author:        josh napoles
 Date:          12/3/17
 Class:         CS300
 Assignment:    HashTables
 Purpose:       This file defines the constants, data structures, and
                function prototypes for implementing a generic hashtable
                data structure.
 *************************************************************************/
#ifndef INCLUDE_HT_H_
#define INCLUDE_HT_H_

#include <stdbool.h>
#include "../../GenericDynamicList/include/list.h"

#define MAX_STR_LEN 16

typedef struct HashTableElement* HashTableElementPtr;
typedef struct HashTableElement
{
	void *pKey;
	void *pData;
} HashTableElement;

// function pointers
typedef int (*Hash) (void *, int);
typedef bool (*compareKey) (void *pKey1, void *pKey2);
typedef void (*printHashElement) (HashTableElementPtr);

typedef struct HashTable* HashTablePtr;
typedef struct HashTable
{
	List *psLists;
	int numBuckets;

	Hash pHashFunction;
	compareKey pCompareFunction;
	printHashElement pPrintFunction;
} HashTable;

extern void htCreate (HashTablePtr psHT, int htSize, Hash pHashFucntion
		, compareKey pCompFunction, printHashElement pPrintFuncition);

extern void htTerminate (HashTablePtr psHT);

extern bool htIsEmpty (HashTablePtr psHT); //
extern bool htIsFull (HashTablePtr psHT); //


extern void htInsertElement (HashTablePtr psHT, void *pKey,
		void *pData, int keySize, int dataSize);

//find functions
extern bool htDeleteElement (HashTablePtr psHT, void *pKey);
extern bool htUpdateElement (HashTablePtr psHT, void *pKey,
		void *pData,int dataSize);

extern HashTableElement htSearch (HashTablePtr psHT, void *pKey,
		int pKeySize, int dataSize,bool *bFound);

extern void printHT (HashTablePtr psHT);
extern int getNumBuckets (HashTablePtr psHT);

#endif /* INCLUDE_HT_H_ */
