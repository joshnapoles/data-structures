/**************************************************************************
 File name:  pqueue.c
 Author:     josh napoles
 Date:			 10.30.18
 Class:			 CS300
 Assignment: GenericDynamicPriorityQ
 Purpose:    Interface for a dynamic queue of generic elements
 *************************************************************************/
#include "../include/pqueue.h"
#include <stdio.h>
#include <stdlib.h>

char gszPQErrors [NUMBER_OF_LIST_ERRORS][MAX_ERROR_PQ_CHARS];
/**************************************************************************
 * Function:		pqueueLoadErrorMessages
 *
 * Description:	Loads the error message strings
 * 							for the error handler to use No error conditions
 *
 * Parameters:	none
 *
 * Returned:		none
 *************************************************************************/
void pqueueLoadErrorMessages ()
{
	LOAD_PQ_ERRORS
}
/**************************************************************************
 * Function:		 pqueueCreate
 *
 * Description:	 If PQ can be created, then PQ exists and is empty
 * 							 otherwise, ERROR_NO_PQ_CREATE
 *
 * Parameters:	 psQueue	- pointer to the priority queue
 *
 * Returned:		 none
 *************************************************************************/
void pqueueCreate (PriorityQueuePtr psQueue)
{
	if (NULL == psQueue)
	{
		fprintf (stderr, "%s\n", gszPQErrors [ERROR_NO_PQ_CREATE]);
		exit (0);
	}

	lstCreate (&(psQueue->sTheList));
}
/**************************************************************************
 * Function:		 pqueueSize
 *
 * Description:	 returns the number of elements in the PQ
 * 							 error code priority: ERROR_INVALID_PQ if PQ is NULL
 *
 * Parameters:	 psQueue	- pointer to the priority queue
 *
 * Returned:		 none
 *************************************************************************/
int pqueueSize (const PriorityQueuePtr psQueue)
{
	if (NULL == psQueue)
	{
		fprintf (stderr, "%s\n", gszPQErrors [ERROR_INVALID_PQ]);
		exit (0);
	}

	return lstSize (&(psQueue->sTheList));
}
// results: Returns the number of elements in the PQ
// 					error code priority: ERROR_INVALID_PQ if PQ is NULL

bool pqueueIsFull (const PriorityQueuePtr psQueue)
{
	if (NULL == psQueue)
	{
		fprintf (stderr, "%s\n", gszPQErrors [ERROR_INVALID_PQ]);
		exit (0);
	}

	return lstIsFull (&(psQueue->sTheList));
}
// results: If PQ is full, return true; otherwise, return false
// 					error code priority: ERROR_INVALID_PQ

bool pqueueIsEmpty (const PriorityQueuePtr psQueue)
{
	if (NULL == psQueue)
	{
		fprintf (stderr, "%s\n", gszPQErrors [ERROR_INVALID_PQ]);
		exit (0);
	}

	return lstIsEmpty (&(psQueue->sTheList));
}
// results: If PQ is empty, return true; otherwise, return false
// 					error code priority: ERROR_INVALID_PQ

void pqueueEnqueue (PriorityQueuePtr psQueue, const void *pBuffer,
										int size, int priority)
{
	PriorityQueueElement sE;


	if (NULL == psQueue)
	{
		fprintf (stderr, "%s\n", gszPQErrors [ERROR_INVALID_PQ]);
		exit (0);
	}
	else if (NULL == pBuffer)
	{
		fprintf (stderr, "%s\n", gszPQErrors [ERROR_NULL_PQ_PTR]);
		exit (0);
	}
	else if (pqueueIsFull (psQueue))
	{
		fprintf (stderr, "%s\n", gszPQErrors [ERROR_FULL_PQ]);
		exit (0);
	}

	//psE = malloc (sizeof (PriorityQueueElement));
	sE->pData = malloc (size);

	memcpy (sE.pData, pBuffer, size);
	sE.priority = priority;

	if (pqueueIsEmpty (&(psQueue->sTheList)))
	{
		lstInsertAfter (&(psQueue->sTheList), &sE, sizeof (PriorityQueueElement));
	}
	else
	{

	}

}
// requires: psQueue is not full
// results: Insert the element into the priority queue based on the
//          priority of the element.
//					error code priority: ERROR_INVALID_PQ, ERROR_NULL_PQ_PTR,
//															 ERROR_FULL_PQ



