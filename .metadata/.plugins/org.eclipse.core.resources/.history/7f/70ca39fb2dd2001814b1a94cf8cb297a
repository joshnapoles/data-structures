/**************************************************************************
 File name:  listdriver.c
 Author:     josh napoles
 Date:			 10.17.18
 Class:			 CS300
 Assignment: Generic Dynamic List
 Purpose:    Driver to test list functionality
 Hours:			 15
 *************************************************************************/

#include "../include/list.h"

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <ctype.h>

/**************************************************************************
 * Function:		lstPrintInt
 *
 * Description:	print a List that exclusively has ints
 *
 * Parameters:	psList	-	list pointer
 *
 * Returned:		none
 *************************************************************************/
static void lstPrintInt (ListPtr psList)
{
	ListElementPtr psTemp = psList->psCurrent;
	int buffer;

	lstFirst(psList);
	while (lstHasNext (psList))
	{
		lstPeek(psList, &buffer, sizeof (int));
		printf ("%d->", buffer);
		lstNext(psList);
	}
	lstPeek(psList, &buffer, sizeof (int));
	printf ("%d-|  ", buffer);
	psList->psCurrent = psTemp;
}

/**************************************************************************
 * Function:		success
 *
 * Description:	print a success message
 *
 * Parameters:	pszStr	-	the message to print
 *
 * Returned:		none
 *************************************************************************/
static void success (char *pszStr)
{
	printf ("SUCCESS: %s\n", pszStr);
}
/**************************************************************************
 * Function:		failure
 *
 * Description:	print a failure message
 *
 * Parameters:	pszStr	-	the message to print
 *
 * Returned:		none
 *************************************************************************/
static void failure (char *pszStr)
{
	printf ("FAILURE: %s\n", pszStr);
}
/**************************************************************************
 * Function:		assert
 *
 * Description:	if the expression is true, assert success; otherwise,
 * 							assert failure
 *
 * Parameters:	bExpression  - true or false expression to be asserted
 * 							pszTrue		   - the message to print when true
 * 							pszFalse	   - the message to print when false
 *
 * Returned:		none
 *************************************************************************/
static void assert (bool bExpression, char *pszTrue, char *pszFalse)
{
	if (bExpression)
	{
		success (pszTrue);
	}
	else if (!bExpression)
	{
		failure (pszFalse);
	}
}
/**************************************************************************
 * Function:		main
 *
 * Description:	test all the functionality of the list
 *
 * Parameters:	none
 *
 * Returned:		Exit Status
 *************************************************************************/

#include "../include/list.h"
int main ()
{
	List sList;
	int i;

	puts ("\nProgram Start\nSUCCESS TESTS\n\n");

	lstLoadErrorMessages ();
	lstCreate (&sList);
	assert (0 == lstSize (&sList), "List was Created", "List Was Not Created");

	assert (!lstIsFull (&sList), "List is Not Full", "List is Full");
	assert (lstIsEmpty (&sList), "List is Empty", "List is Not Empty");


	printf ("\nInserting 1 - 5 into the list\n");
	for (i = 1; i < 6; i++)
	{
		lstInsertAfter (&sList, &i, sizeof (int));
	}

	lstPrintInt(&sList);
	printf ("Current %d\n", *(int *) lstPeek(&sList, &i, sizeof (int)));

	assert (5 == lstSize (&sList),
			"List contains 5 elements", "List does not contain 5 elements");

	assert (!lstIsFull (&sList), "List is Not Full", "List is Full");
	assert (!lstIsEmpty (&sList), "List is Not Empty", "List is Empty");

	printf ("\nMoving Current to first\n");
	lstFirst(&sList);

	lstPrintInt(&sList);
	printf ("Current %d\n", *(int *) lstPeek(&sList, &i, sizeof (int)));

	assert (1 == *(int *) lstPeek (&sList, &i, sizeof (int)),
			"Current is 1", "First element is not 1");

	printf ("Peeking into Next Element\n");
	assert (2 == *(int *)lstPeekNext (&sList, &i, sizeof (int)),
			"Next is 2", "Next is Not 2");

	printf ("\nMoving Current to Next\n");
	lstNext (&sList);

	lstPrintInt(&sList);
	printf ("Current %d\n", *(int *) lstPeek(&sList, &i, sizeof (int)));

	assert (2 == *(int *) lstPeek (&sList, &i, sizeof (int)),
			"Current is 2", "Current is not 2");

	printf ("\nMoving Current to Last\n");
	lstLast (&sList);

	lstPrintInt(&sList);
	printf ("Current %d\n", *(int *) lstPeek(&sList, &i, sizeof (int)));

	assert (5 == *(int *) lstPeek (&sList, &i, sizeof (int)),
			"Current is 5", "Current is not 5");

	if (lstHasCurrent (&sList) && lstHasNext (&sList))
	{
		lstNext(&sList);
	}

	//test delete current

	printf ("\nDeleting Current\n");
	lstDeleteCurrent (&sList, &i, sizeof (int));

	lstPrintInt(&sList);
	printf ("Current %d\n", *(int *) lstPeek(&sList, &i, sizeof (int)));
	assert (4 == lstSize (&sList),
			"Current Deleted", "Current Not Deleted");

	i = 99;
	printf ("\nInserting Before First Element\n");
	lstFirst(&sList);
	lstInsertBefore(&sList, &i, sizeof (int));

	lstPrintInt(&sList);
	printf ("Current %d\n", *(int *) lstPeek(&sList, &i, sizeof (int)));
	assert (5 == lstSize (&sList),
			"Element Inserted", "Element Not Inserted");

	printf ("\nUpdating Current Element to 0\n");
	i = 0;
	lstUpdateCurrent (&sList, &i, sizeof (int));

	lstPrintInt(&sList);
	printf ("Current %d\n", *(int *) lstPeek(&sList, &i, sizeof (int)));
	assert (0 == *(int *)lstPeek (&sList, &i, sizeof (int)),
				"Current Updated to 0", "Current Not Updated to 0");

	lstTerminate (&sList);
	puts ("\nProgram End\n");
	return EXIT_SUCCESS;
}
