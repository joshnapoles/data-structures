/**************************************************************************
 File name:  pqueuedriver.c
 Author:     josh napoles
 Date:			 10.30.18
 Class:			 CS300
 Assignment: Generic Dynamic list
 Purpose:    Interface for a dynamic list of generic elements
 *********************** updated for airport******************************
 *************************************************************************/

#include "../include/pqueue.h"

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <ctype.h>


/**************************************************************************
 * Function:		success
 *
 * Description:	print a success message
 *
 * Parameters:	pszStr	-	the message to print
 *
 * Returned:		none
 *************************************************************************/
static void success (char *pszStr)
{
	printf ("SUCCESS: %s\n", pszStr);
}
/**************************************************************************
 * Function:		failure
 *
 * Description:	print a failure message
 *
 * Parameters:	pszStr	-	the message to print
 *
 * Returned:		none
 *************************************************************************/
static void failure (char *pszStr)
{
	printf ("FAILURE: %s\n", pszStr);
}
/**************************************************************************
 * Function:		assert
 *
 * Description:	if the expression is true, assert success; otherwise,
 * 							assert failure
 *
 * Parameters:	bExpression  - true or false expression to be asserted
 * 							pszTrue		   - the message to print when true
 * 							pszFalse	   - the message to print when false
 *
 * Returned:		none
 *************************************************************************/
static void assert (bool bExpression, char *pszTrue, char *pszFalse)
{
	if (bExpression)
	{
		success (pszTrue);
	}
	else if (!bExpression)
	{
		failure (pszFalse);
	}
}/**************************************************************************
 * Function:		assert
 *
 * Description:	tests the functionality of the priority queue
 *
 * Parameters: 	none
 *
 * Returned:		none
 *************************************************************************/
int main ()
{
	PriorityQueue sQ;
	char ch = 'a';
	int priority, i;

	puts ("\nProgram Start\nSUCCESS TESTS\n\n");

	pqueueLoadErrorMessages ();

	pqueueCreate (&sQ);
	assert (0 == pqueueSize(&sQ), "PQ was Created", "PQ was not Created");
	assert (pqueueIsEmpty(&sQ), "PQ is Empty", "PQ is Not Empty");
	assert (!pqueueIsFull(&sQ), "PQ is Not Full", "PQ is Full");

	puts ("\nEnqueuing a-e with priority 1-5");
	for (i = 1; i < 6; i++)
	{
		pqueueEnqueue (&sQ, &ch, sizeof (char), i);
		ch++;
	}
	assert (!pqueueIsEmpty(&sQ), "PQ is Not Empty", "PQ is Empty");
	assert (!pqueueIsFull(&sQ), "PQ is Not Full", "PQ is Full");
	assert (5 == pqueueSize(&sQ), "5 Elements in PQ", "5 Elements Not in PQ");

	pqueuePeek (&sQ, &ch, sizeof (char), &priority);
	printf ("\nPeeking into first Element\n"
			"%c With Priority %d \n", ch, priority);


	puts ("\nDequeuing");
	for (i = 1; i < 5; i++)
	{
		pqueueDequeue (&sQ, &ch, sizeof (char), &priority);
		printf ("Deleted %c With Priority %d from the PQ\n", ch, priority);
	}

	pqueueTerminate(&sQ);
	assert (pqueueIsEmpty(&sQ), "PQ is Empty", "PQ is Not Empty");
	// size

	pqueueCreate (&sQ);
	puts ("\nEnqueuing a-j with priority 10-1");
	ch = 'a';
	for (i = 10; i > 0; i--)
	{
		pqueueEnqueue (&sQ, &ch, sizeof (char), i);
		ch++;
	}

	assert (10 == pqueueSize(&sQ), "10 Elements in PQ", "10 Elements Not in PQ");

	puts ("\nChanging Priority of all elements by 100");
	pqueueChangePriority (&sQ, 100);

	pqueuePeek (&sQ, &ch, sizeof (char), &priority);
	printf ("\nPeeking into first Element\n"
			"%c With Priority %d \n", ch, priority);

	puts ("\nDequeuing");
	for (i = 0; i < 10; i++)
	{
		pqueueDequeue (&sQ, &ch, sizeof (char), &priority);
		printf ("Deleted %c With Priority %d from the PQ\n", ch, priority);
	}

	assert (pqueueIsEmpty(&sQ), "PQ is Empty", "PQ is Not Empty");

	pqueueTerminate(&sQ);
	pqueueCreate (&sQ);

	puts ("\nEnqueuing A-E with priority 1-5");
	ch = 'A';
	for (i = 1; i  < 6; i++)
	{
		pqueueEnqueue (&sQ, &ch, sizeof (char), i);
		ch++;
	}
	assert (5 == pqueueSize(&sQ), "5 Elements in PQ", "5 Elements Not in PQ");

	puts ("\nEnqueuing a-e with priority 1-5");
	ch = 'a';
	for (i = 1; i  < 6; i++)
	{
		pqueueEnqueue (&sQ, &ch, sizeof (char), i);
		ch++;
	}

	assert (10 == pqueueSize(&sQ), "10 Elements in PQ", "10 Elements Not in PQ");

	puts ("Dequeuing");
	for (i = 0; i < 10; i++)
	{
		pqueueDequeue (&sQ, &ch, sizeof (char), &priority);
		printf ("Deleted %c With Priority %d from the PQ\n", ch, priority);
	}

	assert (pqueueIsEmpty(&sQ), "PQ is Empty", "PQ is Not Empty");

	return EXIT_SUCCESS;
}
