/**************************************************************************
 File name:  pqueue.c
 Author:     josh napoles
 Date:			 10.31.18
 Class:			 CS300
 Assignment: GenericDynamicPriorityQ
 Purpose:    Interface for a dynamic queue of generic elements
 *************************************************************************/
#include "../include/pqueue.h"
//#include "../../GenericDynamicList/include/list.h"
#include <stdio.h>
#include <stdlib.h>

char gszPQErrors [NUMBER_OF_LIST_ERRORS][MAX_ERROR_PQ_CHARS];
/**************************************************************************
 * Function:		pqueueLoadErrorMessages
 *
 * Description:	Loads the error message strings
 * 							for the error handler to use No error conditions
 *
 * Parameters:	none
 *
 * Returned:		none
 *************************************************************************/
void pqueueLoadErrorMessages ()
{
	LOAD_PQ_ERRORS
}
/**************************************************************************
 * Function:		 pqueueCreate
 *
 * Description:	 If PQ can be created, then PQ exists and is empty
 * 							 otherwise, ERROR_NO_PQ_CREATE
 *
 * Parameters:	 psQueue	- pointer to the priority queue
 *
 * Returned:		 none
 *************************************************************************/
void pqueueCreate (PriorityQueuePtr psQueue)
{
	if (NULL == psQueue)
	{
		fprintf (stderr, "%s\n", gszPQErrors [ERROR_NO_PQ_CREATE]);
		exit (0);
	}

	lstCreate (&(psQueue->sTheList));
}
/**************************************************************************
 * Function:		 pqueueSize
 *
 * Description:	 returns the number of elements in the PQ
 * 							 error code priority: ERROR_INVALID_PQ if PQ is NULL
 *
 * Parameters:	 psQueue	- pointer to the priority queue
 *
 * Returned:		 none
 *************************************************************************/
int pqueueSize (const PriorityQueuePtr psQueue)
{
	if (NULL == psQueue)
	{
		fprintf (stderr, "%s\n", gszPQErrors [ERROR_INVALID_PQ]);
		exit (0);
	}

	return lstSize (&(psQueue->sTheList));
}
/**************************************************************************
 * Function:		 pqueueIsFull
 *
 * Description:	 If PQ is full, return true; otherwise, return false
 * 							 error code priority: ERROR_INVALID_PQ
 *
 * Parameters:	 psQueue	- pointer to the priority queue
 *
 * Returned:		 bool	- true if full; false otherwise
 *************************************************************************/
bool pqueueIsFull (const PriorityQueuePtr psQueue)
{
	if (NULL == psQueue)
	{
		fprintf (stderr, "%s\n", gszPQErrors [ERROR_INVALID_PQ]);
		exit (0);
	}

	return lstIsFull (&(psQueue->sTheList));
}
/**************************************************************************
 * Function:		 pqueueIsEmpty
 *
 * Description:	 If PQ is Empty, return true; otherwise, return false
 * 							 error code priority: ERROR_INVALID_PQ
 *
 * Parameters:	 psQueue	- pointer to the priority queue
 *
 * Returned:		 bool	- true if Empty; false otherwise
 *************************************************************************/
bool pqueueIsEmpty (const PriorityQueuePtr psQueue)
{
	if (NULL == psQueue)
	{
		fprintf (stderr, "%s\n", gszPQErrors [ERROR_INVALID_PQ]);
		exit (0);
	}

	return lstIsEmpty (&(psQueue->sTheList));
}
/**************************************************************************
 * Function:		 pqueueEnqueue
 *
 * Description:	 Insert the element into the priority queue based on the
 * 							 priority of the element.
 *
 * Parameters:	 psQueue	- pointer to the priority queue
 * 							 pBuffer	- data to be Enqueued
 * 							 size			- size of the data to be enqueued
 * 							 priority	- priority of the Element
 *
 * Returned:		 none
 *************************************************************************/
void pqueueEnqueue (PriorityQueuePtr psQueue, const void *pBuffer,
										int size, int priority)
{
	PriorityQueueElement sE; // inserted element
	PriorityQueueElement sTemp;
	PriorityQueueElement sTemp2;
	bool bIsEnqueued = false;
	bool bInsertAfter = false, bInsertBefore = false;

	if (NULL == psQueue)
	{
		fprintf (stderr, "%s\n", gszPQErrors [ERROR_INVALID_PQ]);
		exit (0);
	}
	else if (NULL == pBuffer)
	{
		fprintf (stderr, "%s\n", gszPQErrors [ERROR_NULL_PQ_PTR]);
		exit (0);
	}
	else if (pqueueIsFull (psQueue))
	{
		fprintf (stderr, "%s\n", gszPQErrors [ERROR_FULL_PQ]);
		exit (0);
	}
	sE.pData = malloc (size);

	memcpy (sE.pData, pBuffer, size);
	sE.priority = priority;

	if (pqueueIsEmpty (psQueue))
	{
		lstInsertAfter (&(psQueue->sTheList), &sE, sizeof (PriorityQueueElement));
	}
	else
	{
		lstFirst (&(psQueue->sTheList));
		while (!bIsEnqueued)
		{
			lstPeek (&(psQueue->sTheList), &sTemp, sizeof (PriorityQueueElement));

			if (sTemp.priority > sE.priority)
			{
				bInsertBefore = true;
			}
			else if (sTemp.priority <= sE.priority)
			{
				if (lstHasNext (&(psQueue->sTheList)))
				{
					lstPeekNext (&(psQueue->sTheList), &sTemp2, sizeof (PriorityQueueElement));
					if (sTemp2.priority > sE.priority)
					{
						bInsertAfter = true;
					}
				}
				else
				{
					bInsertAfter = true;
				}
			}

			if (bInsertBefore)
			{
				lstInsertBefore (&(psQueue->sTheList), &sE, sizeof (PriorityQueueElement));
				bIsEnqueued = true;
				//printf ("%d inserted before %d\n ", sE.priority, sTemp.priority);
			}
			else if (bInsertAfter)
			{
				lstInsertAfter (&(psQueue->sTheList), &sE, sizeof (PriorityQueueElement));
				bIsEnqueued = true;
				//printf ("%d inserted after %d\n ", sE.priority, sTemp.priority);
			}
			else
			{
				lstNext (&(psQueue->sTheList));
			}
		}

	}

}
/**************************************************************************
 * Function:		 pqueueDequeue
 *
 * Description:	 Remove the first element from the priority queue
 *
 * Parameters:	 psQueue	- pointer to the priority queue
 * 							 pBuffer	- pointer to data to removed from the PQ
 * 							 size			- size of data to be removed
 * 							 priority	- priority of the removed Element
 *
 * Returned:		 void*		- data that was dequeued
 *************************************************************************/
void *pqueueDequeue (PriorityQueuePtr psQueue, void *pBuffer,
														int size, int  *pPriority)
{
	PriorityQueueElement sTemp;
	if (NULL == psQueue)
	{
		fprintf (stderr, "%s\n", gszPQErrors [ERROR_INVALID_PQ]);
		exit (0);
	}
	else if (NULL == pBuffer || NULL == pPriority)
	{
		fprintf (stderr, "%s\n", gszPQErrors [ERROR_NULL_PQ_PTR]);
		exit (0);
	}
	else if (lstIsEmpty (&(psQueue->sTheList)))
	{
		fprintf (stderr, "%s\n", gszPQErrors [ERROR_EMPTY_PQ]);
		exit (0);
	}


	lstFirst (&(psQueue->sTheList));

	lstDeleteCurrent (&(psQueue->sTheList), &sTemp,
			sizeof (PriorityQueueElement));

	*pPriority = sTemp.priority;
	memcpy (pBuffer, sTemp.pData, size);
	free (sTemp.pData);

	return pBuffer;
}
/**************************************************************************
 * Function:		 pqueuePeek
 *
 * Description:	 Peek into the first element of the PQ
 *
 * Parameters:	 psQueue	- pointer to the priority queue
 * 							 pBuffer	- pointer to data of the first element
 * 							 size			- size of data
 * 							 priority	- priority of the element
 *
 * Returned:		 void*		- pointer to the data of the first element
 *************************************************************************/
extern void *pqueuePeek (PriorityQueuePtr psQueue, void *pBuffer, int size,
								 int *priority)
{
	PriorityQueueElement sTemp;
	if (NULL == psQueue)
	{
		fprintf (stderr, "%s\n", gszPQErrors [ERROR_INVALID_PQ]);
		exit (0);
	}
	else if (NULL == pBuffer || NULL == priority)
	{
		fprintf (stderr, "%s\n", gszPQErrors [ERROR_NULL_PQ_PTR]);
		exit (0);
	}
	else if (lstIsEmpty (&(psQueue->sTheList)))
	{
		fprintf (stderr, "%s\n", gszPQErrors [ERROR_EMPTY_PQ]);
		exit (0);
	}

	lstFirst (&(psQueue->sTheList));

	lstPeek (&(psQueue->sTheList), &sTemp,
			sizeof (PriorityQueueElement));

	*priority = sTemp.priority;
	memcpy (pBuffer, sTemp.pData, size);

	return pBuffer;
}
/**************************************************************************
 * Function:		 pqueueChangePriority
 *
 * Description:	 Adds the change to the priority of all elements
 *
 * Parameters:	 psQueue	- pointer to the priority queue
 * 							 change		- change to be added to priority of all elements
 *
 * Returned:		 none
 *************************************************************************/
void pqueueChangePriority (PriorityQueuePtr psQueue,
																	int change)
{
	PriorityQueueElement sTemp;
	if (NULL == psQueue)
	{
		fprintf (stderr, "%s\n", gszPQErrors [ERROR_INVALID_PQ]);
		exit (0);
	}

	if (!lstIsEmpty (&(psQueue->sTheList)) || 0 != change)
	{
		lstFirst (&(psQueue->sTheList));

		while (lstHasNext(&(psQueue->sTheList)))
		{
			lstPeek (&(psQueue->sTheList), &sTemp, sizeof (PriorityQueueElement));
			sTemp.priority += change;
			lstUpdateCurrent (&(psQueue->sTheList), &sTemp,
					sizeof (PriorityQueueElement));

			lstNext (&(psQueue->sTheList));
		}

		lstPeek (&(psQueue->sTheList), &sTemp, sizeof (PriorityQueueElement));
		sTemp.priority += change;
		lstUpdateCurrent (&(psQueue->sTheList), &sTemp,
				sizeof (PriorityQueueElement));

	}
}
/**************************************************************************
 * Function:		 pqueueChangePriority
 *
 * Description:	 Adds the change to the priority of all elements
 *
 * Parameters:	 psQueue	- pointer to the priority queue
 * 							 change		- change to be added to priority of all elements
 *
 * Returned:		 none
 *************************************************************************/
void pqueueTerminate (PriorityQueuePtr psQueue)
{
	PriorityQueueElement sTemp;
	if (NULL == psQueue)
	{
		fprintf (stderr, "%s\n", gszPQErrors [ERROR_INVALID_PQ]);
		exit (0);
	}

	while (!lstIsEmpty (& (psQueue->sTheList)))
	{
		lstDeleteCurrent (&psQueue->sTheList, &sTemp,
				sizeof (PriorityQueueElement));
		free (sTemp.pData);
	}

	lstTerminate (&psQueue->sTheList);
}
