#############################################################################
# File name:	Makefile
# Author:		Josh Napoles	
# Date:			10/3/18
# Class:		CS 300
# Assignment:	GenericDynamicStack
# Purpose:		compile files for GenericStatic stack driver
#############################################################################


CC=gcc
CFLAGS=-g -Wall

# -g  include debug symbols in the executable so that the code can be
# 		run through the debugger effectively
#
# -Wall	show all warnings from gcc


.PHONY: clean all tarball


TARGETS= bin/genstkdriver bin/stkdriver

all: ${TARGETS}
bin/stkdriver: bin/stkdriver.o bin/stk.o
	${CC} -o bin/stkdriver ${CFLAGS} bin/stkdriver.o bin/stk.o
	
bin/genstkdriver: bin/genstkdriver.o bin/stk.o
	${CC} -o bin/genstkdriver ${CFLAGS} bin/genstkdriver.o bin/stk.o
	
bin/genstkdriver.o: src/genstkdriver.c include/stk.h
	${CC} -o bin/genstkdriver.o ${CFLAGS} src/genstkdriver.c -c	
	
bin/stkdriver.o: src/stkdriver.c include/stk.h
	${CC} -o bin/stkdriver.o ${CFLAGS} src/stkdriver.c -c
	
bin/stk.o: src/stk.c include/stk.h
	${CC} -o bin/stk.o ${CFLAGS} src/stk.c -c
	
clean:
	rm -f ${TARGETS} bin/*.o
	
tarball: clean
	tar czf ~/cs300_3_napo2208.tar.gz ../GenericDynamicStack
	
valgrind:
	valgrind -v --leak-check=full --show-leak-kinds=all bin/stkdriver

valgrindgen:
	valgrind -v --leak-check=full --show-leak-kinds=all bin/genstkdriver
	