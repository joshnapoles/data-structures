/**************************************************************************
 File name:  stkdriver.c
 Author:     josh napoles
 Date:			 10.03.18
 Class:			 CS300
 Assignment: Generic Dynamic Stack
 Purpose:    This driver will open a file and read lines of possible
             palindromes. If the word is a palindrome, then the word
             followed by palindrome will be printed; otherwise, the word
             followed by not palindrome will be printed.
 *************************************************************************/


#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <ctype.h>

#include "../include/stk.h"

/**************************************************************************
 Function: 	 	isPalindrome

 Description: Takes an existing stack of characters and checks to see
 	 	 	 	 	 	 	whether the characters are a palindrome using a second stack.
 	 	 	 	 	 	 	Half of the first stack of characters are copied into an
 	 	 	 	 	 	 	auxiliary stack and then the characters of both stacks are
 	 	 	 	 	 	 	popped off one at a time and compared.

 	 	 	 	 	 	 	Important: Both stacks are changed!!! If the original stack
 	 	 	 	 	 	 	is to be maintained, then a stkCopy function needs to be
 	 	 	 	 	 	 	written.

 Parameters:	psStack    - pointer to a stack

 Returned:	 	true if the stack holds a palindrome; otherwise, false is
 	 	 	 	 	 	 	returned
 *************************************************************************/
bool isPalindrome (StackPtr psStack)
{
	bool bPalindrome = true;
	Stack sAuxileryStk;
	int i, stackSize = stkSize (psStack);
	char poppedChar;
	char auxChar;

	stkCreate (&sAuxileryStk);
	for (i  = 0; i < (stackSize / 2); i++)
	{
		stkPop (psStack, &poppedChar, 1);
		stkPush (&sAuxileryStk, &poppedChar, 1);
	}

	if (stkSize (psStack) != stkSize(&sAuxileryStk))
	{
		stkPop (psStack, &poppedChar, 1);
	}

	while (!stkIsEmpty(psStack))
	{
		stkPop(psStack, &poppedChar, 1);
		stkPop (&sAuxileryStk, &auxChar, 1);
		if (poppedChar != auxChar)
		{
			bPalindrome = false;
		}
	}

	stkTerminate (&sAuxileryStk);
	return bPalindrome;

}

/**************************************************************************
 Function: 	 	main

 Description: The driver will open a file and read one line at a time from
              the file placing each alphabetic (upper and lowercase)
              character on the stack ignoring all other characters. The
              original line of data is written to the screen followed by
              [palindrome] or [not palindrome]. The final line in the data
              file MUST have a newline for this program to work correctly.

 Parameters:	None

 Returned:	 	Exit Status
 *************************************************************************/
int main ()
{
	FILE *pFile;
	char ch;
	Stack sStack;

	stkLoadErrorMessages();
	printf ("PALINDROME CHECKER\n");
	printf ("---------- -------\n\n");

	pFile = fopen ("datafiles/palindromes.txt", "r");
	if (NULL == pFile)
	{
		printf ("Error: File Not Open");
	}
	else
	{
		stkCreate (&sStack);

		while ((ch = fgetc (pFile)) != EOF)
		{

			if (isalpha (ch))
			{
				printf ("%c", ch);
				ch = tolower (ch);
				stkPush (&sStack, &ch, sizeof (char));
			}
			else if ('\n' == ch)
			{
				printf ("%c", ' ');
				if (isPalindrome (&sStack))
				{
					printf ("[palindrome]\n");
				}
				else
				{
					printf ("[not palindrome]\n");
				}
				stkTerminate (&sStack);
				stkCreate (&sStack);
			}
			else
			{
				printf ("%c", ch);
			}
		}
		stkTerminate (&sStack);
	}

	fclose (pFile);
	return EXIT_SUCCESS;
}
