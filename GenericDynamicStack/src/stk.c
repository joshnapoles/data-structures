/**************************************************************************
 File name:  stk.c
 Author:     josh napoles
 Date:			 10.03.18
 Class:			 CS300
 Assignment: Generic Dynamic Stack
 Purpose:    Interface for a dynamic stack of generic elements
 *************************************************************************/
#include "../include/stk.h"
#include <stdio.h>
#include <stdlib.h>

#define MAX_ERROR_CHARS 64
char gszErrors [STK_NUMBER_OF_ERRORS][MAX_ERROR_CHARS];

/**************************************************************************
 * Function:		stkLoadErrorMessages
 *
 * Description:	load the stack error messages
 *
 * Parameters:	none
 *
 * Returned:		none
 *************************************************************************/
void stkLoadErrorMessages ()
{
	LOAD_ERRORS
}
/**************************************************************************
 * Function:		stkCreate
 *
 * Description:	create a stack
 *
 * Parameters:	psStack	-	pointer to stack that is to be created
 *
 * Returned:		none
 *************************************************************************/
void stkCreate (StackPtr psStack)
{
	if (NULL == psStack)
	{
		printf ("%s\n", gszErrors [STK_NO_CREATE_ERROR]);
		exit (0);
	}


	psStack->size = 0;
	psStack->psTop = NULL;
}
/**************************************************************************
 * Function:		stkIsFull
 *
 * Description:	checks to see if the stack is full
 *
 * Parameters:	psStack	-	pointer to stack that is to be checked
 *
 * Returned:		bool	- true if stack is full false otherwise
 *************************************************************************/
bool stkIsFull (const StackPtr psStack)
{
	if (NULL == psStack)
	{
		printf ("%s\n", gszErrors [STK_NO_CREATE_ERROR]);
		exit (0);
	}
	return false;
}
/**************************************************************************
 * Function:		stkIsEmpty
 *
 * Description:	checks to see if the stack is empty
 *
 * Parameters:	psStack	-	pointer to stack that is to be checked
 *
 * Returned:		bool	- true if stack is empty; false otherwise
 *************************************************************************/
bool stkIsEmpty (const StackPtr psStack)
{
	if (NULL == psStack)
	{
		printf ("%s\n", gszErrors [STK_NO_CREATE_ERROR]);
		exit (0);
	}

	bool bIsEmpty = false;
	if (0 == stkSize(psStack))
	{
		bIsEmpty = true;
	}
	return bIsEmpty;
}
/**************************************************************************
 * Function:		stkSize
 *
 * Description:	returns the number of elements in the stack
 *
 * Parameters:	psStack	-	pointer to stack that is to be checked
 *
 * Returned:		int 		-	size of stack
 *************************************************************************/
int stkSize (const StackPtr psStack)
{
	if (NULL == psStack)
	{
		printf ("%s\n", gszErrors [STK_NO_CREATE_ERROR]);
		exit (0);
	}
	return psStack->size;
}
/**************************************************************************
 * Function:		stkPush
 *
 * Description:	Pushes an element to the top of the stack
 *
 * Parameters:	psStack	-	the stack
 * 							pBuffer	-	the element that will be pushed on the stack
 * 							size		-	the size of the element
 *
 * Returned:		none
 *************************************************************************/
void stkPush (StackPtr psStack, void *pBuffer, int size)
{
	if (NULL == psStack)
	{
		printf ("%s\n", gszErrors [STK_NO_CREATE_ERROR]);
		exit (0);
	}
	NodePtr psTemp;
	if (!stkIsFull (psStack))
	{
		if (NULL == psStack->psTop)
		{
			psStack->psTop = malloc (sizeof (Node));
			psStack->psTop->pData = malloc (size);
			memcpy (psStack->psTop->pData, pBuffer, size);
			psStack->psTop->psNext = NULL;
		}
		else
		{
			psTemp = malloc (sizeof (Node));
			psTemp->pData = malloc (size);
			memcpy (psTemp->pData,pBuffer, size);
			psTemp->psNext = psStack->psTop;
			psStack->psTop = psTemp;
		}

		psStack->size++;
	}
	else
	{
		fprintf (stderr, "%s\n", gszErrors[STK_FULL_ERROR]);
		exit (0);
	}

}
/**************************************************************************
 * Function:		stkPop
 *
 * Description:	Pops an element from the top of the stack
 *
 * Parameters:	psStack	-	the stack
 * 							pBuffer	-	element that is popped off the stack
 * 							size		- size of the popped off element
 *
 * Returned:		void* - pointer the the popped off element
 *************************************************************************/
void *stkPop (StackPtr psStack, void *pBuffer, int size)
{
	if (NULL == psStack)
	{
		printf ("%s\n", gszErrors [STK_NO_CREATE_ERROR]);
		exit (0);
	}
	NodePtr psTemp;
	if (!stkIsEmpty (psStack))
	{
		psTemp = psStack->psTop->psNext;
		memcpy (pBuffer, psStack->psTop->pData, size);
		free (psStack->psTop->pData);
		free (psStack->psTop);
		psStack->psTop = psTemp;
		psStack->size--;
	}
	else
	{
		fprintf (stderr, "%s\n", gszErrors[STK_EMPTY_ERROR]);
		exit (0);
	}

	return pBuffer;
}
/**************************************************************************
 * Function:		stkTerminate
 *
 * Description:	terminates the stack
 *
 * Parameters:	psStack	-	the stack to be terminated
 *
 * Returned:		none
 *************************************************************************/
void stkTerminate (StackPtr psStack)
{
	if (NULL == psStack)
	{
		printf ("%s\n", gszErrors [STK_NO_CREATE_ERROR]);
		exit (0);
	}
	NodePtr psTemp;

	if (!stkIsEmpty (psStack))
	{
		while (NULL != psStack->psTop->psNext)
		{
			psTemp = psStack->psTop->psNext;
			free (psStack->psTop->pData);
			free (psStack->psTop);
			psStack->psTop = psTemp;
		}
		free (psStack->psTop->pData);
		free (psStack->psTop);
		psStack->size = 0;
	}
}
/**************************************************************************
 * Function:		stkPeek
 *
 * Description:	Peeks at the top of the stack
 *
 * Parameters:	psStack	-	pointer to stack that is to be checked
 * 							pBuffer	-	pointer to the top element of the stack
 *
 *
 * Returned:		void*		-	pointer to the top element of the stack
 *************************************************************************/
void *stkPeek (const StackPtr psStack, void *pBuffer, int size)
{
	if (NULL == psStack)
	{
		printf ("%s\n", gszErrors [STK_NO_CREATE_ERROR]);
		exit (0);
	}
	if (!stkIsEmpty (psStack))
	{
		memcpy (pBuffer, psStack->psTop->pData, size);
	}
	else
	{
		fprintf (stderr, "%s\n", gszErrors[STK_EMPTY_ERROR]);
		exit (0);
	}

	return pBuffer;
}


