/**************************************************************************
 File name:  queue.c
 Author:     josh napoles
 Date:			 11/14/18
 Class:			 CS300
 Assignment: Airport
 Purpose:    Interface for a dynamic queue of generic elements
 *************************************************************************/
#include "../include/queue.h"
#include "../../GenericDynamicPriorityQ/include/pqueue.h"
#include <stdio.h>
#include <stdlib.h>

char gszQErrors [NUMBER_OF_LIST_ERRORS][MAX_ERROR_PQ_CHARS];
/**************************************************************************
 * Function:		pqueueLoadErrorMessages
 * Description:	Loads the error message strings
 * 							for the error handler to use No error conditions
 * Parameters:	none
 *
 * Returned:		none
 *************************************************************************/
void queueLoadErrorMessages ()
{
	LOAD_Q_ERRORS
}
/**************************************************************************
 * Function:		queueCreate
 * Description:	creates a queue
 * Parameters:	psQueue	- the queue
 *
 * Returned:		none
 *************************************************************************/
void queueCreate (QueuePtr psQueue)
{
	if (NULL == psQueue)
	{
		fprintf (stderr, "%s\n", gszQErrors [ERROR_NO_Q_CREATE]);
		exit (0);
	}

	pqueueCreate (&psQueue->sTheQueue);
}

/**************************************************************************
 * Function:		queueCreate
 * Description:	returns the size of the queue
 * Parameters:	psQueue	- the queue
 *
 * Returned:		int		- size
 *************************************************************************/
int queueSize (const QueuePtr psQueue)

{
	if (NULL == psQueue)
	{
		fprintf (stderr, "%s\n", gszQErrors [ERROR_INVALID_Q]);
		exit (0);
	}

	return pqueueSize (&psQueue->sTheQueue);
}
/**************************************************************************
 * Function:		queueIsFull
 * Description:	returns true if the queue is full
 * Parameters:	psQueue	- the queue
 *
 * Returned:		bool 	- true if full; false otherwise
 *************************************************************************/
bool queueIsFull (const QueuePtr psQueue)
{
	if (NULL == psQueue)
	{
		fprintf (stderr, "%s\n", gszQErrors [ERROR_INVALID_Q]);
		exit (0);
	}

	return pqueueIsFull (&psQueue->sTheQueue);
}
/**************************************************************************
 * Function:		queueIsEmpty
 * Description: returns true if the queue is empty
 * Parameters:	psQueue	- the queue
 *
 * Returned:		bool		- true if empty; false otherwise
 *************************************************************************/
bool queueIsEmpty (const QueuePtr psQueue)
{
	if (NULL == psQueue)
	{
		fprintf (stderr, "%s\n", gszQErrors [ERROR_INVALID_Q]);
		exit (0);
	}

	return pqueueIsEmpty (&psQueue->sTheQueue);
}
/**************************************************************************
 * Function:		queueEnqueue
 * Description: enqueues an element
 * Parameters:	psQueue	- the queue
 *							pBuffer	- data to be enqueue
 *							size		- size of data
 *
 * Returned:		none
 *************************************************************************/
void queueEnqueue (QueuePtr psQueue, const void *pBuffer, int size)
{
	if (NULL == psQueue)
	{
		fprintf (stderr, "%s\n", gszQErrors [ERROR_INVALID_Q]);
		exit (0);
	}
	else if (NULL == pBuffer)
	{
		fprintf (stderr, "%s\n", gszQErrors [ERROR_NULL_Q_PTR]);
		exit (0);
	}
	else if (queueIsFull (psQueue))
	{
		fprintf (stderr, "%s\n", gszQErrors [ERROR_FULL_Q]);
		exit (0);
	}

	pqueueEnqueue (&psQueue->sTheQueue, pBuffer, size, 0);
}
/**************************************************************************
 * Function:		queueDequeue
 * Description: removes an element from the queue
 * Parameters:	psQueue	- the queue
 *
 * Returned:		void*		- pointer ro removed data
 *************************************************************************/
void *queueDequeue (QueuePtr psQueue, void *pBuffer, int size)
{
	int priority;
	if (NULL == psQueue)
	{
		fprintf (stderr, "%s\n", gszQErrors [ERROR_INVALID_Q]);
		exit (0);
	}
	else if (NULL == pBuffer)
	{
		fprintf (stderr, "%s\n", gszQErrors [ERROR_NULL_Q_PTR]);
		exit (0);
	}
	else if (queueIsEmpty (psQueue))
	{
		fprintf (stderr, "%s\n", gszQErrors [ERROR_EMPTY_Q]);
		exit (0);
	}


	pqueueDequeue (&psQueue->sTheQueue, pBuffer, size, &priority);

	return pBuffer;
}
/**************************************************************************
 * Function:		queuePeek
 * Description: peeks at the first element in the queu
 * Parameters:	psQueue	- the queue
 * 							pBuffer	- the data of the first element in queue
 * 							size		- size of the data to be peeked from queue
 *
 * Returned:		void*		- the data of the first element in 	queue
 *************************************************************************/
void *queuePeek (QueuePtr psQueue, void *pBuffer, int size)
{
	int priority;
	if (NULL == psQueue)
	{
		fprintf (stderr, "%s\n", gszQErrors [ERROR_INVALID_Q]);
		exit (0);
	}
	else if (NULL == pBuffer)
	{
		fprintf (stderr, "%s\n", gszQErrors [ERROR_NULL_Q_PTR]);
		exit (0);
	}
	else if (queueIsEmpty (psQueue))
	{
		fprintf (stderr, "%s\n", gszQErrors [ERROR_EMPTY_Q]);
		exit (0);
	}

	pqueuePeek(&psQueue->sTheQueue, pBuffer, size, &priority);

	return pBuffer;
}
/**************************************************************************
 * Function:		queueTerminate
 * Description: terminates the queue
 * Parameters:	psQueue	- the queue
 *
 * Returned:		none
 *************************************************************************/
void queueTerminate (QueuePtr psQueue)
{
	if (NULL == psQueue)
	{
		fprintf (stderr, "%s\n", gszQErrors [ERROR_NO_Q_TERMINATE]);
		exit (0);
	}

	pqueueTerminate(&psQueue->sTheQueue);
}

