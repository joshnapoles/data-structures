/**************************************************************************
 File name:  queuedriver.c
 Author:     josh napoles
 Date:			 11/14/18
 Class:			 CS300
 Assignment: airport
 Purpose:    Interface for a dynamic q of generic elements
 *************************************************************************/

#include "../include/queue.h"

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <ctype.h>


/**************************************************************************
 * Function:		success
 *
 * Description:	print a success message
 *
 * Parameters:	pszStr	-	the message to print
 *
 * Returned:		none
 *************************************************************************/
static void success (char *pszStr)
{
	printf ("SUCCESS: %s\n", pszStr);
}
/**************************************************************************
 * Function:		failure
 *
 * Description:	print a failure message
 *
 * Parameters:	pszStr	-	the message to print
 *
 * Returned:		none
 *************************************************************************/
static void failure (char *pszStr)
{
	printf ("FAILURE: %s\n", pszStr);
}
/**************************************************************************
 * Function:		assert
 *
 * Description:	if the expression is true, assert success; otherwise,
 * 							assert failure
 *
 * Parameters:	bExpression  - true or false expression to be asserted
 * 							pszTrue		   - the message to print when true
 * 							pszFalse	   - the message to print when false
 *
 * Returned:		none
 *************************************************************************/
static void assert (bool bExpression, char *pszTrue, char *pszFalse)
{
	if (bExpression)
	{
		success (pszTrue);
	}
	else if (!bExpression)
	{
		failure (pszFalse);
	}
}/**************************************************************************
 * Function:		assert
 *
 * Description:	tests the functionality of the priority queue
 *
 * Parameters: 	none
 *
 * Returned:		none
 *************************************************************************/
int main ()
{
	Queue sQ;
	int val = 1;
	int num;

	puts ("PROGRAM START");
	queueLoadErrorMessages ();
	queueCreate (&sQ);
	assert (0 == queueSize(&sQ), "Q size is 0", "Q size is not 2");
	assert (queueIsEmpty (&sQ), "Q is Empty", "Q is not Empty");
	assert (!queueIsFull (&sQ), "Q is Not Full","Q is Full");

	queueEnqueue (&sQ, &val, sizeof (int));
	val++;
	queueEnqueue (&sQ, &val, sizeof (int));
	val++;

	queueEnqueue (&sQ, &val, sizeof (int));

	assert (3 == queueSize(&sQ), "Q size is 3", "Q size is not 3");
	assert (!queueIsEmpty (&sQ), "Q is not Empty", "Q is Empty");
	assert (!queueIsFull (&sQ), "Q is Not Full","Q is Full");

	puts ("Peeking");
	queuePeek (&sQ, &num, sizeof (int));
	printf ("Peeked val: %d\n", num);

	queueDequeue (&sQ, &num, sizeof (int));
	printf ("%d\n", num);
	queueDequeue (&sQ, &num, sizeof (int));
	printf ("%d\n", num);

	queueTerminate (&sQ);

	queueCreate (&sQ);
	queueEnqueue (&sQ, &val, sizeof (int));
	queueTerminate (&sQ);

	puts ("PROGRAM END");
	return EXIT_SUCCESS;
}
