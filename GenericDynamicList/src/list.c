/**************************************************************************
 File name:  list.c
 Author:     josh napoles
 Date:			 10.17.18
 Class:			 CS300
 Assignment: Generic Dynamic list
 Purpose:    Interface for a dynamic list of generic elements
 ************************UPDATED FOR PQUEUE*******************************
 *************************************************************************/
#include "../include/list.h"
#include <stdio.h>
#include <stdlib.h>


#define MAX_ERROR_CHARS 64
char gszListErrors [NUMBER_OF_LIST_ERRORS][MAX_ERROR_CHARS];

/**************************************************************************
 * Function:		lstLoadErrorMessages
 *
 * Description:	Loads the error message strings
 * 							for the error handler to use No error conditions
 *
 * Parameters:	none
 *
 * Returned:		none
 *************************************************************************/
void lstLoadErrorMessages ()
{
	LOAD_LIST_ERRORS
}

/**************************************************************************
 * Function:		lstCreate
 *
 * Description:	If the list can be created, then the list exists and is
 *							empty; otherwise, ERROR_NO_LIST_CREATE if psList is NULL
 *
 * Parameters:	psList -	pointer to the list
 *
 * Returned:		none
 *************************************************************************/
void lstCreate (ListPtr psList)
{
	if (psList == NULL)
	{
		fprintf (stderr, "%s\n", gszListErrors[ERROR_NO_LIST_CREATE]);
		exit (0);
	}

	psList->psFirst = NULL;
	psList->psLast = NULL;
	psList->psCurrent = NULL;

	psList->numElements = 0;
}
/**************************************************************************
 * Function:		lstSize
 *
 * Description:	 Returns the number of elements in the list
 * 							 error code priority: ERROR_INVALID_LIST
 *
 * Parameters:	psList -	pointer to the list
 *
 * Returned:		int	-	the size of the list
 *************************************************************************/
int lstSize (const ListPtr psList)
{
	if (psList == NULL)
		{
			fprintf (stderr, "%s\n", gszListErrors[ERROR_INVALID_LIST]);
			exit (0);
		}

	return psList->numElements;
}
/**************************************************************************
 * Function:		lstIsFull
 *
 * Description:	If list is full, return true; otherwise, return false
 * 							error code priority: ERROR_INVALID_LIST
 *
 * Parameters:	psList -	pointer to the list
 *
 * Returned:		bool	- true if the list is full; false otherwise
 *************************************************************************/
bool lstIsFull (const ListPtr psList)
{
	bool bIsFull = false;

	if (psList == NULL)
		{
			fprintf (stderr, "%s\n", gszListErrors[ERROR_INVALID_LIST]);
			exit (0);
		}

	return bIsFull;
}
/**************************************************************************
 * Function:		lstIsEmpty
 *
 * Description:	If list is empty, return true; otherwise, return false
 * 							error code priority: ERROR_INVALID_LIST
 *
 * Parameters:	psList -	pointer to the list
 *
 * Returned:		bool	- true if the list is empty; false otherwise
 *************************************************************************/
bool lstIsEmpty (const ListPtr psList)
{
	bool bIsEmpty = false;

	if (psList == NULL)
		{
			fprintf (stderr, "%s\n", gszListErrors[ERROR_INVALID_LIST]);
			exit (0);
		}

	if (0 == psList->numElements)
	{
		bIsEmpty = true;
	}

	return bIsEmpty;
}
/**************************************************************************
 * Function:		lstInsertAfter
 *
 * Description:	If the list is not empty, insert the new element as the
 *  						successor of the current element and make the inserted
 *  						element the current element; otherwise, insert element
 *  						and make it current.
 *
 * Parameters:	psList 	-	pointer to the list
 * 							pBuffer	- data to be inserted
 * 							size		-	size of the data
 *
 * Returned:		none
 *************************************************************************/
void lstInsertAfter (ListPtr psList, const void *pBuffer, int size)
{
	ListElementPtr psTemp;
	if (psList == NULL)
	{
		fprintf (stderr, "%s\n", gszListErrors [ERROR_INVALID_LIST]);
		exit (0);
	}
	else if (lstIsFull (psList))
	{
		fprintf (stderr, "%s\n", gszListErrors [ERROR_FULL_LIST]);
		exit (0);
	}
	else if (NULL == pBuffer)
	{
		fprintf (stderr, "%s\n", gszListErrors [ERROR_NULL_PTR]);
		exit (0);
	}

	psTemp = malloc (sizeof (ListElement));
	psTemp->pData = malloc (size);
	memcpy (psTemp->pData, pBuffer, size);

	if (lstIsEmpty (psList))
	{
		psTemp->psNext = NULL;

		psList->psCurrent = psTemp;
		psList->psLast = psTemp;
		psList->psFirst = psTemp;
	}
	else
	{
		psTemp->psNext = psList->psCurrent->psNext;
		psList->psCurrent->psNext = psTemp;
		psList->psCurrent = psTemp;

		if (NULL == psList->psCurrent->psNext)
		{
			psList->psLast = psList->psCurrent;
		}
	}
	psList->numElements++;
}
/**************************************************************************
 * Function:		lstTerminate
 *
 * Description:	If the list can be terminated, then the list no longer
 * 							exists and is empty; otherwise, ERROR_NO_LIST_TERMINATE
 *
 * Parameters:	psList 	-	pointer to the list
 *
 * Returned:		none
 *************************************************************************/
void lstTerminate (ListPtr psList)
{
	if (NULL == psList)
	{
		fprintf (stderr, "%s\n", gszListErrors [ERROR_NO_LIST_TERMINATE]);
		exit (0);
	}

	psList->psCurrent = psList->psFirst;

	while (psList->psCurrent != NULL)
	{
		psList->psCurrent = psList->psFirst->psNext;
		free (psList->psFirst->pData);
		free (psList->psFirst);
		psList->psFirst = psList->psCurrent;
	}

	psList->psFirst = NULL;
	psList->psLast = NULL;
	psList->psCurrent = NULL;
	psList->numElements = 0;
}
/**************************************************************************
 * Function:		lstFirst
 *
 * Description:	If the list is not empty, current is changed to the first
 *  						element error code priority
 *  						ERROR_INVALID_LIST, ERROR_EMPTY_LIST
 *
 * Parameters:	psList 	-	pointer to the list
 *
 * Returned:		none
 *************************************************************************/
void lstFirst (ListPtr psList)
{
	if (NULL == psList)
	{
		fprintf (stderr, "%s\n", gszListErrors [ERROR_INVALID_LIST]);
		exit (0);
	}

	if (!lstIsEmpty (psList))
	{
		psList->psCurrent = psList->psFirst;
	}
	else
	{
		fprintf (stderr, "%s\n", gszListErrors [ERROR_EMPTY_LIST]);
		exit (0);
	}
}
/**************************************************************************
 * Function:		lstNext
 *
 * Description:	 If the list is not empty, current is changed to the
 * 							 successor of the current element error code priority:
 * 							 ERROR_INVALID_LIST, ERROR_EMPTY_LIST, ERROR_NO_CURRENT
 *
 * Parameters:	psList 	-	pointer to the list
 *
 * Returned:		none
 *************************************************************************/
void lstNext (ListPtr psList)
{
	if (NULL == psList)
	{
		fprintf (stderr, "%s\n", gszListErrors [ERROR_INVALID_LIST]);
		exit (0);
	}
	if (NULL == psList->psCurrent)
	{
		fprintf (stderr, "%s\n", gszListErrors [ERROR_NO_CURRENT]);
		exit (0);
	}

	if (!lstIsEmpty (psList))
	{
		psList->psCurrent = psList->psCurrent->psNext;
	}
	else
	{
		fprintf (stderr, "%s\n", gszListErrors [ERROR_EMPTY_LIST]);
		exit (0);
	}
}
/**************************************************************************
 * Function:		lstPeek
 *
 * Description:	 The value of the current element is returned
 * 							 Does not change current
 *
 * Parameters:	psList 	-	pointer to the list
 *
 * Returned:		The data value of current is returned
 *************************************************************************/
void *lstPeek (const ListPtr psList, void *pBuffer, int size)
{
	if (NULL == psList)
	{
		fprintf (stderr, "%s\n", gszListErrors [ERROR_INVALID_LIST]);
		exit (0);
	}

	if (!lstIsEmpty (psList))
	{
		memcpy (pBuffer, psList->psCurrent->pData, size);
	}
	else
	{
		fprintf (stderr, "%s\n", gszListErrors [ERROR_EMPTY_LIST]);
		exit (0);
	}

	//
	if (NULL == pBuffer)
	{
		fprintf (stderr, "%s\n", gszListErrors [ERROR_NO_CURRENT]);
		exit (0);
	}


	return pBuffer;
}
/**************************************************************************
 * Function:		lstHasCurrent
 *
 * Description: Returns true if the current node is not NULL; otherwise,
 * 							false is returned error code priority: ERROR_INVALID_LIST
 *
 * Parameters:	psList 	-	pointer to the list
 *
 * Returned:		none
 *************************************************************************/
bool lstHasCurrent (const ListPtr psList)
{
	bool bHasCurrent = true;
	if (NULL == psList)
	{
		fprintf (stderr, "%s\n", gszListErrors [ERROR_INVALID_LIST]);
		exit (0);
	}

	if (NULL == psList->psCurrent)
	{
		bHasCurrent = false;
	}


	return bHasCurrent;
}
/**************************************************************************
 * Function:		lstHasNext
 *
 * Description:	Returns true if the current node has a successor;
 * 							otherwise, false is returned error code priority:
 * 							ERROR_INVALID_LIST
 *
 * Parameters:	psList 	-	pointer to the list
 *
 * Returned:		none
 *************************************************************************/
bool lstHasNext (const ListPtr psList)
{
	bool bHasNext = true;

	if (NULL == psList)
	{
		fprintf (stderr, "%s\n", gszListErrors [ERROR_INVALID_LIST]);
		exit (0);
	}

	if (lstHasCurrent(psList))
	{
		if (NULL == psList->psCurrent->psNext)
		{
			bHasNext = false;
		}
	}
	else
	{
		bHasNext = false;
	}

	return bHasNext;
}
/**************************************************************************
 * Function:		lstPeekNext
 *
 * Description:	List contains two or more elements and current
 * 							is not last	The data value of current's successor
 * 							is returnedDo not change current error code
 * 							priority: ERROR_INVALID_LIST, ERROR_NULL_PTR,
 * 							ERROR_EMPTY_LIST, ERROR_NO_CURRENT, ERROR_NO_NEXT
 *
 * Parameters:	psList 	-	pointer to the list
 * 							pBuffer - the data of the successor
 * 							size		- size of the data of the successor
 *
 * Returned:		The data value of current's successor is returned
 *************************************************************************/
extern void *lstPeekNext (const ListPtr psList, void *pBuffer, int size)
{
	if (NULL == psList)
	{
		fprintf (stderr, "%s\n", gszListErrors [ERROR_INVALID_LIST]);
		exit (0);
	}

	// error checking
	if (2 > lstSize (psList))
	{
		fprintf (stderr, "%s\n", gszListErrors [ERROR_NULL_PTR]);
		exit (0);
	}
	else if (lstIsEmpty(psList))
	{
		fprintf (stderr, "%s\n", gszListErrors [ERROR_EMPTY_LIST]);
		exit (0);
	}
	else if (!lstHasCurrent(psList))
	{
		fprintf (stderr, "%s\n", gszListErrors [ERROR_NO_CURRENT]);
		exit (0);
	}
	else if (!lstHasNext(psList))
	{
		fprintf (stderr, "%s\n", gszListErrors [ERROR_NO_NEXT]);
		exit (0);
	}

	memcpy (pBuffer, psList->psCurrent->psNext->pData, size);

	return pBuffer;
}
/**************************************************************************
 * Function:		lstLast
 *
 * Description:	List is not empty If the list is not empty,
 * 							current is changed to last if it exists error code
 * 							priority: ERROR_INVALID_LIST, ERROR_EMPTY_LIST
 *
 * Parameters:	psList 	-	pointer to the list
 *
 * Returned:		none
 *************************************************************************/
void lstLast (ListPtr psList)
{
	if (NULL == psList)
	{
		fprintf (stderr, "%s\n", gszListErrors [ERROR_INVALID_LIST]);
		exit (0);
	}

	if (!lstIsEmpty (psList))
	{
		psList->psCurrent = psList->psLast;
	}
	else
	{
		fprintf (stderr, "%s\n", gszListErrors [ERROR_EMPTY_LIST]);
		exit (0);
	}
}
/**************************************************************************
 * Function:		lstDeleteCurrent
 *
 * Description:	The current element is deleted and its successor
 * 							and	predecessor become each others successor and
 * 							predecessor. If the deleted element had a predecessor,
 * 							then make it the new current element; otherwise, make the
 * 							first element current if it exists. The value of the
 * 							deleted element is returned.error code priority:
 * 							ERROR_INVALID_LIST, ERROR_NULL_PTR,
 * 							ERROR_EMPTY_LIST, ERROR_NO_CURRENT
 *
 * Parameters:	psList 	-	pointer to the list
 * 							pBuffer - pointer to the deleted data
 * 							int			- size of the deleted element
 *
 * Returned:		void * 	- pointer to the deleted element
 *************************************************************************/
void *lstDeleteCurrent (ListPtr psList, void *pBuffer, int size)
{
	ListElementPtr psTemp;
	if (NULL == psList)
	{
		fprintf (stderr, "%s\n", gszListErrors [ERROR_INVALID_LIST]);
		exit (0);
	}

	if (lstIsEmpty (psList))
	{
		fprintf (stderr, "%s\n", gszListErrors [ERROR_EMPTY_LIST]);
		exit (0);
	}
	else if (!lstHasCurrent (psList))
	{
		fprintf (stderr, "%s\n", gszListErrors [ERROR_NO_CURRENT]);
		exit (0);
	}

	memcpy (pBuffer, psList->psCurrent->pData, size);
	psTemp = psList->psCurrent;

	if (1 != lstSize (psList) && psList->psFirst != psTemp)
	{
		lstFirst (psList);
		while (psList->psCurrent->psNext != psTemp)
		{
			lstNext (psList);
		}

		psList->psCurrent->psNext = psTemp->psNext;
	}
	else if (psList->psFirst == psTemp)
	{
		psList->psCurrent = psList->psCurrent->psNext;
		psList->psFirst = psList->psCurrent;
	}

	if (psTemp == psList->psLast)
	{
		psList->psLast = psList->psCurrent;
	}

	free (psTemp->pData);
	free (psTemp);
	psTemp = NULL;
	psList->numElements--;;

	if (lstIsEmpty (psList))
	{
		psList->psCurrent = NULL;
		psList->psLast = NULL;
		psList->psFirst = NULL;
	}

	return pBuffer;
}
/**************************************************************************
 * Function:		lstInsertBefore
 *
 * Description:	If the list is not empty, insert the new element as the
 * 							predecessor of the current element and make the inserted
 * 							element the current element; otherwise, insert element
 * 							and make it current.error code priority:
 * 							ERROR_INVALID_LIST, ERROR_NULL_PTR,ERROR_NO_CURRENT
 *
 * Parameters:	psList 	-	pointer to the list
 * 							pBuffer - pointer to inserted data
 * 							int			- size of the inserted element
 *
 * Returned:		none
 *************************************************************************/
void lstInsertBefore (ListPtr psList, const void *pBuffer,
														 int size)
{
	ListElementPtr psTemp;
	if (NULL == psList)
	{
		fprintf (stderr, "%s\n", gszListErrors [ERROR_INVALID_LIST]);
		exit (0);
	}
	else if (NULL == pBuffer)
	{
		fprintf (stderr, "%s\n", gszListErrors [ERROR_NULL_PTR]);
		exit (0);
	}
	else if (lstIsFull (psList))
	{
		fprintf (stderr, "%s\n", gszListErrors [ERROR_FULL_LIST]);
		exit (0);
	}


	psTemp = malloc (sizeof (ListElement));
	psTemp->pData = malloc (size);
	memcpy (psTemp->pData, pBuffer, size);

	if (lstIsEmpty (psList))
	{
		psTemp->psNext = NULL;

		psList->psCurrent = psTemp;
		psList->psLast = psTemp;
		psList->psFirst = psTemp;
	}
	else
	{
		psTemp->psNext = psList->psCurrent;

		if (psTemp->psNext != psList->psFirst)
		{
			lstFirst (psList);
			while (psList->psCurrent->psNext != psTemp->psNext)
			{
				lstNext (psList);
			}

			psList->psCurrent->psNext = psTemp;
		}
		else
		{
			psList->psFirst = psTemp;
		}

	}
	psList->psCurrent = psTemp;
	psList->numElements++;

}
/**************************************************************************
 * Function:		lstUpdateCurrent
 *
 * Description: The value of pBuffer is copied into the current element
 * 							error code priority: ERROR_INVALID_LIST,
 * 							 ERROR_NULL_PTR,ERROR_EMPTY_LIST, ERROR_NO_CURRENT
 *
 * Parameters:	psList 	-	pointer to the list
 * 							pBuffer - pointer to updated data
 * 							int			- size of the updated data
 *
 * Returned:		none
 *************************************************************************/
void lstUpdateCurrent (ListPtr psList, const void *pBuffer,
														  int size)
{
	if (NULL == psList)
	{
		fprintf (stderr, "%s\n", gszListErrors [ERROR_INVALID_LIST]);
		exit (0);
	}
	else if (NULL == pBuffer)
	{
		fprintf (stderr, "%s\n", gszListErrors [ERROR_NULL_PTR]);
		exit (0);
	}
	else if (lstIsEmpty (psList))
	{
		fprintf (stderr, "%s\n", gszListErrors [ERROR_EMPTY_LIST]);
		exit (0);
	}
	else if (NULL == psList->psCurrent)
	{
		fprintf (stderr, "%s\n", gszListErrors [ERROR_NO_CURRENT]);
		exit (0);
	}


	psList->psCurrent->pData = realloc (psList->psCurrent->pData, size);
	memcpy (psList->psCurrent->pData, pBuffer, size);

}


