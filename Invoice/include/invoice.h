/**************************************************************************
 File name:		  invoice.h
 Author:        josh napoles
 Date:          12/3/17
 Class:         CS300
 Assignment:    HashTables
 Purpose:       This file defines the constants, data structures, and
                function prototypes for implementing an item and action
 *************************************************************************/
#ifndef INCLUDE_INVOICE_H_
#define INCLUDE_INVOICE_H_

#include <stdbool.h>
#include <stdio.h>
#include "../../HashTable/include/ht.h"

#define MAX_DENOM_LEN 3
typedef struct Item* ItemPtr;
typedef struct Item
{
	char product [MAX_STR_LEN];
	char brand [MAX_STR_LEN];
}Item;

#define MAX_ITEM_LEN 75
typedef struct Action* ActionPtr;
typedef struct Action
{
	int productID;
	int quantity;
	double cost;
	char denom [MAX_DENOM_LEN];
}Action;



extern void itemCreate (ItemPtr psItem, char *product, char *brand);

extern void insertItem (HashTablePtr psHT, int *ID ,ItemPtr psItem);
extern void insertConversion (HashTablePtr psHT, char *denom, double *rate);

extern void printItemToBuf (ItemPtr, char*);

extern void printInvoiceLine (HashTablePtr, HashTablePtr, ActionPtr , FILE*);


#endif /* INCLUDE_INVOICE_H_ */
