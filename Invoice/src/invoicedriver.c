/**************************************************************************
 File name:  invoicedriver.c
 Author:     josh napoles
 Date:			 12.3.18
 Class:			 CS300
 Assignment: HashTable
 Purpose:    Interface for an invoice
 *************************************************************************/
#include "../include/invoice.h"
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <ctype.h>

/**************************************************************************
 * Function:		HashInt
 *
 * Description:	returns a bucket using midsquare
 *
 * Parameters:	pKey		- ptr to integer key to be hashed
 * 							htSize	- number of buckets in the hash table
 *
 * Returned:		int			- bucket that key hashes to
 *************************************************************************/
int HashInt (void *pKey, int htSize)
{
	unsigned int key = *(int*) pKey;
	unsigned int middle;
	int bucket;

	if (NULL == pKey)
	{
		fprintf (stderr, "ERROR: INVALID KEY\n");
		exit (0);
	}

	key *= key;
	key = key << 8;
	middle = (key & 0x000ff000) >> 12;
	bucket = middle % htSize;

	return bucket;
}
/**************************************************************************
 * Function:		compareIntKey
 *
 * Description:	compares two integer keys
 *
 * Parameters:	pKey1	- integer key ptr
 * 							pKey2	- integer key ptr
 *
 * Returned:		true if keys are equivalent; false otherwise
 *************************************************************************/
bool compareIntKey (void *pKey1, void *pKey2)
{
	bool bSame = false;
	if (*(int*) pKey1 == *(int*)pKey2)
	{
		bSame = true;
	}

	return bSame;
}
/**************************************************************************
 * Function:		printIntElement
 *
 * Description:	prints a HashTableElement of form KEY:{int} DATA: {Item}
 *
 * Parameters:	HashTableElementPtr	- ptr to HashTableElement
 *
 * Returned:		none
 *************************************************************************/
void printIntElement (HashTableElementPtr psElement)
{
	char item [MAX_ITEM_LEN];
	if (NULL == psElement)
	{
		fprintf (stderr, "ERROR: INVALID ELEMENT\n");
		exit (0);
	}

		printf ("K:%d D: ", *(int*) psElement->pKey);
		printItemToBuf ((ItemPtr) psElement->pData, item);
		printf ("%s", item);
}

/**************************************************************************
 * Function:		HashString
 *
 * Description:	hashes a string to a bucket in the HashTable
 *
 * Parameters:	pKey1	- integer key ptr
 * 							htSize- size of the HashTable
 *
 * Returned:		int		- bucket that maps to hashed string
 *************************************************************************/
int HashString (void *pKey, int htSize)
{
	int value = 0;
	char *h;
	for (int i = 0; i < MAX_DENOM_LEN; i++)
	{
		h = (char*) pKey;

		value = (*h) * 31 + value;
	}

	value %= htSize;

	return value;
}
/**************************************************************************
 * Function:		compareStringKey
 *
 * Description:	compares two string keys
 *
 * Parameters:	pKey1	- string key ptr
 * 							pKey2	- string key ptr
 *
 * Returned:		true if strings are equivalent; false otherwise
 *************************************************************************/
bool compareStringKey (void *pKey1 , void *pKey2)
{
	bool bSameKey = false;

	if (0 == strncmp ((char*)pKey1, (char*)pKey2, MAX_DENOM_LEN))
	{
		bSameKey = true;
	}

	return bSameKey;
}
/**************************************************************************
 * Function:		printStringElement
 *
 * Description:	prints a HashTableElement of form KEY:{string} DATA:{double}
 *
 * Parameters:	HashTableElementPtr	- ptr to HashTableElement
 *
 * Returned:		none
 *************************************************************************/
void printStringElement (HashTableElementPtr psElement)
{
	if (NULL == psElement)
	{
		fprintf (stderr, "ERROR: INVALID ELEMENT\n");
		exit (0);
	}

	printf ("K:%s D:%.2lf", (char*) psElement->pKey, *(double*) psElement->pData);
}


/**************************************************************************
 * Function:		main
 *
 * Description:	tests the functionality of the priority queue
 *
 * Parameters: 	none
 *
 * Returned:		EXIT_SUCCESS
 *************************************************************************/
#define MAX_INVOICE_LINE 75
#define MAX_NAME 16
int main ()
{
	//Key: Product ID (int) Data: Item (struct)
	HashTable sItemHashTable;
	int itemHTSize = 7;


	//Key: Denomination (string) Data: rate (float)
	HashTable sConversionHashTable;
	int conversionHTSize = 5;

	//file I/0
	FILE *inventory, *conversions ,*actions, *invoice;


	// Item variables
	Item sItem;
	int productID;
	char product[MAX_STR_LEN];
	char brand[MAX_STR_LEN];

	//Conversion variables
	char denom[MAX_DENOM_LEN];
	double rate;

	//action variables
	Action sA;


	puts ("PROGRAM START\n");
	htCreate (&sItemHashTable, itemHTSize,&HashInt, &compareIntKey, &printIntElement);
	htCreate (&sConversionHashTable, conversionHTSize, &HashString, &compareStringKey, &printStringElement);

	//open files
	inventory = fopen ("data/items.txt", "r");
	if (NULL == inventory)
	{
		fprintf (stderr, "Unable to open file %s", "data/items.txt");
		exit (0);
	}

	conversions = fopen ("data/conversions.txt", "r");
	if (NULL == conversions)
	{
		fprintf (stderr, "Unable to open file %s", "data/conversions.txt");
		exit (0);
	}

	actions = fopen ("data/actions.txt", "r");
	if (NULL == conversions)
	{
		fprintf (stderr, "Unable to open file %s", "data/actions.txt");
		exit (0);
	}

	invoice = fopen ("data/invoice.txt", "w");
	if (NULL == conversions)
	{
		fprintf (stderr, "Unable to open file %s", "data/invoice.txt");
		exit (0);
	}

	while (EOF != fscanf (inventory,"%d", &productID))
	{
		memset (product, 0, MAX_STR_LEN);
		memset (brand, 0, MAX_STR_LEN);
		fscanf (inventory, "%s %s", product, brand);

		strcpy (sItem.product, product);
		strcpy (sItem.brand, brand);

		insertItem (&sItemHashTable, &productID, &sItem);
	}

	memset (denom, 0, MAX_DENOM_LEN);
	while (EOF != fscanf (conversions, "%s", denom))
	{
		fscanf (conversions, "%lf", &rate);

		insertConversion (&sConversionHashTable, denom, &rate);
		memset (denom, 0, MAX_DENOM_LEN);
	}


	// uncomment to show both hash tables printed to stdout

	/*puts("");
	printHT(&sItemHashTable);
	puts ("");

	puts("");
	printHT(&sConversionHashTable);
	puts ("");*/

	while (EOF != fscanf (actions, "%d", &sA.productID))
	{
		fscanf (actions, "%d %lf %s", &sA.quantity, &sA.cost, sA.denom);

		printInvoiceLine (&sItemHashTable, &sConversionHashTable, &sA,invoice);
	}


	// close files
	fclose (inventory);
	fclose (conversions);
	fclose (actions);
	fclose (invoice);


	htTerminate (&sConversionHashTable);
	htTerminate (&sItemHashTable);
	puts ("PROGRAM END");
	return EXIT_SUCCESS;
}
