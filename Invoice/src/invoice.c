/**************************************************************************
 File name:  invoice.c
 Author:     josh napoles
 Date:			 12.3.18
 Class:			 CS300
 Assignment: HashTable
 Purpose:    Interface for a invoice
 *************************************************************************/
#include "../include/invoice.h"
#include <stdlib.h>
#include <stdio.h>
/**************************************************************************
 * Function:		 itemCreate
 *
 * Description:	 creates an item
 *
 * Parameters:	 psItem	-	ptr to item
 * 							 product- product string
 * 							 brand	- brand string
 *
 * Returned:		 none
 *************************************************************************/
void itemCreate (ItemPtr psItem, char *product, char *brand)
{

	if (NULL == psItem)
	{
		fprintf (stderr, "ERROR: INVALID ITEM PTR\n");
		exit (0);
	}
	else if (NULL == product)
	{
		fprintf (stderr, "ERROR: INVALID PRODUCT\n");
		exit (0);
	}
	else if (NULL == brand)
	{
		fprintf (stderr, "ERROR: INVALID Brand\n");
		exit (0);
	}
	else if (strlen(product) > MAX_STR_LEN)
	{
		fprintf (stderr, "ERROR: PRODUCT NAME TOO LONG\n");
		exit (0);
	}
	else if (strlen(brand) > MAX_STR_LEN)
	{
		fprintf (stderr, "ERROR: BRAND NAME TOO LONG\n");
		exit (0);
	}


	memset (psItem->product, 0, MAX_STR_LEN);
	memset (psItem->brand, 0, MAX_STR_LEN);

	strcpy (psItem->product, product);
	strcpy (psItem->product, product);
}
/**************************************************************************
 * Function:		 itemCreate
 *
 * Description:	 inserts an item into the hash table
 *
 * Parameters:	 psHT		-	ptr to item hashTable
 * 							 ID			- ptr to  int product ID {KEY}
 * 							 psItem	- ptr to item
 *
 * Returned:		 none
 *************************************************************************/
void insertItem (HashTablePtr psHT, int *ID, ItemPtr psItem)
{
	if (NULL == psHT)
	{
		fprintf (stderr, "ERROR: INVALID HASH TABLE PTR\n");
		exit (0);
	}
	else if (NULL == psItem)
	{
		fprintf (stderr, "ERROR: INVALID ITEM PTR\n");
		exit (0);
	}

	if (!htIsFull (psHT))
	{
		htInsertElement(psHT, ID, psItem, sizeof (int), sizeof (Item));
	}
}
/**************************************************************************
 * Function:		 itemCreate
 *
 * Description:	 inserts a conversion rate into hashtable
 *
 * Parameters:	 psHT		-	ptr to conversion hashTable
 * 							 deom		- ptr to currency denomination {KEY}
 * 							 rate		- ptr to conversion rate
 *
 * Returned:		 none
 *************************************************************************/
void insertConversion (HashTablePtr psHT, char *denom, double *rate)
{
	if (NULL == psHT)
	{
		fprintf (stderr, "ERROR: INVALID HASH TABLE PTR\n");
		exit (0);
	}
	else if (NULL == denom)
	{
		fprintf (stderr, "ERROR: INVALID DENOM PTR\n");
		exit (0);
	}
	else if (NULL == rate)
	{
		fprintf (stderr, "ERROR: INVALID RATE PTR\n");
		exit (0);
	}


	if (!htIsFull (psHT))
	{
		htInsertElement (psHT, denom, rate, sizeof (MAX_DENOM_LEN),
				sizeof (double));
	}
}
/**************************************************************************
 * Function:		 printItemToBuf
 *
 * Description:	 returns a ptr to a string containing item product&brand
 *
 * Parameters:	 item		- item buffer
 * 							 psItem	- ptr to item
 *
 * Returned:		 none
 *************************************************************************/
void printItemToBuf (ItemPtr psItem, char *item)
{
	if (NULL == psItem)
	{
		fprintf (stderr, "ERROR: INVALID ITEM PTR\n");
		exit (0);
	}

	memset (item, 0, MAX_ITEM_LEN);
	sprintf (item, "%s, %s", psItem->product, psItem->brand);
}

/**************************************************************************
 * Function:		 printInvoiceLine
 *
 * Description:	 prints invoice to file based on the action
 *
 * Parameters:	 psItemsHT				-	ptr to items hashTable
 * 							 psConversionHT		-	ptr to conversion hashTable
 * 							 psA							- ptr to action
 * 							 outfile					- file to print invoice line to
 *
 * Returned:		 none
 *************************************************************************/
void printInvoiceLine (HashTablePtr psItemsHT, HashTablePtr psConversionHT,
		ActionPtr psA, FILE *outfile)
{
	char item [MAX_ITEM_LEN];
	double costInUSD;
	double totalCostInUSD;
	HashTableElement sHE, sHE2;
	Item sItem;
	bool bFound, bFound2;

	if (NULL == psItemsHT)
	{
		fprintf (stderr, "ERROR: INVALID ITEM HASH TABLE PTR\n");
		exit (0);
	}
	else if (NULL == psConversionHT)
	{
		fprintf (stderr, "ERROR: INVALID CONVERSION TABLE PTR\n");
		exit (0);
	}
	else if (NULL == psA)
	{
		fprintf (stderr, "ERROR: INVALID ACTION PTR\n");
		exit (0);
	}
	else if (NULL == outfile)
	{
		fprintf (stderr, "ERROR: INVALID OUTFILE PTR\n");
		exit (0);
	}
	memset (item, 0, MAX_ITEM_LEN);
	sHE = htSearch (psItemsHT, &psA->productID, sizeof (int),
			sizeof (HashTableElement), &bFound);
	sItem = *(Item*) sHE.pData;


	sHE2 = htSearch (psConversionHT, &psA->denom, MAX_DENOM_LEN,
			sizeof (HashTableElement), &bFound2);

	costInUSD = psA->cost * (*(double*) sHE2.pData);

	totalCostInUSD = costInUSD * psA->quantity;

	if (bFound && bFound2)
	{
		printItemToBuf(&sItem, item);

		fprintf (outfile, "%d %s ", psA->productID, item);

		fprintf (outfile, "%d %.2lf %.2lf\n", psA->quantity, costInUSD,
				totalCostInUSD);
	}
	else
	{
		fprintf (stderr, "ERROR: ACTION ITEM NOT FOUND IN HASHTABLE\n");
	}

}


