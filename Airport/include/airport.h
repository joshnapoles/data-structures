/**************************************************************************
 File name:		  airport.h
 Author:        josh napoles
 Date:          11/14/18
 Class:         CS300
 Assignment:    Airport
 Purpose:       This file defines the constants, data structures, and
                function prototypes for implementing a airport datastucture
 *************************************************************************/
#ifndef AIRPORT_H_
#define AIRPORT_H_

#include <stdbool.h>
#include "../../GenericDynamicQ/include/queue.h"
#include "../../GenericDynamicPriorityQ/include/pqueue.h"

typedef struct AirportStats
{
	int totalTakeOffPlanes;
	int totalLandingPlanes;
	int fuelAtLanding;
	int totalLandingWaitTime;
	int totalTakeoffWaitTime;

	int numCrashes;
	int numZeroFuelPlanes;
} AirportStats;

#define MAX_RUNWAYS 3
typedef struct Airport *AirportPtr;
typedef struct Airport
{
	PriorityQueue sLandingQ;
	Queue sTakeoffQ;
	AirportStats sStats;

	char aRunway [MAX_RUNWAYS];
} Airport;


extern void createAirport (AirportPtr psAirport);
extern void terminateAirport (AirportPtr psAirport);
extern void createStats (AirportStats *psStats);
extern void printHeader ();

extern bool bQueuesEmpty (AirportPtr psAirport);

extern void enqueueTakeoffPlane (AirportPtr psAirport);
extern void enqueueLandingPlane (AirportPtr psAirport, int fuel);
extern char landPlane (AirportPtr psAirport);
extern char planetakeOff (AirportPtr psAirport);


extern void assignRunways (AirportPtr psAirport);
extern void resetRunways (AirportPtr psAirport);
extern void getRunway (AirportPtr psAirport, char aRunway[]);
extern int crashPlanes (AirportPtr psAirport);

extern int takeoffQSize(AirportPtr psAirport);
extern int landingQSize (AirportPtr psAirport);


extern void reduceFuel (AirportPtr psAirport);
extern void increaseLandingWaitTime (AirportPtr psAirport);
extern void increaseTakeoffWaitTime (AirportPtr psAirport);

extern void printStats (AirportPtr psAirport);



#endif /* INCLUDE_AIRPORT_H_ */
