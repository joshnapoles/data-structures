/**************************************************************************
 File name:		  airport.c
 Author:        josh napoles
 Date:          11/14/18
 Class:         CS300
 Assignment:    Airport
 Purpose:       Airport interface implimentation
 *************************************************************************/
#include "../include/airport.h"
#include "../../GenericDynamicQ/include/queue.h"
#include "../../GenericDynamicPriorityQ/include/pqueue.h"

#include <stdio.h>
#include <stdlib.h>
/**************************************************************************
 * Function:		createAirport
 * Description:	creates an Airport data structure
 * Parameters:	psAirport	- pointer to airport
 *
 * Returned:		none
 *************************************************************************/
void createAirport (AirportPtr psAirport)
{
	if (NULL == psAirport)
	{
		fprintf (stderr, "ERROR: NULL AIRPORT PTR\n");
		exit (0);
	}

	queueCreate (&psAirport->sTakeoffQ);
	pqueueCreate (&psAirport->sLandingQ);
	createStats (&psAirport->sStats);
}
/**************************************************************************
 * Function:		createAirport
 * Description:	initializes Stats data structure in the airport
 * Parameters:	psAirport	- pointer to airport
 *
 * Returned:		none
 *************************************************************************/
void createStats (AirportStats *psStats)
{
	if (NULL == psStats)
	{
		fprintf (stderr, "ERROR: NULL STATS PTR\n");
		exit (0);
	}

	psStats->totalTakeOffPlanes = 0;
	psStats->totalLandingPlanes = 0;
	psStats->fuelAtLanding = 0;
	psStats->totalLandingWaitTime = 0;
	psStats->totalTakeoffWaitTime = 0;

	psStats->numCrashes = 0;
	psStats->numZeroFuelPlanes = 0;
}
/**************************************************************************
 * Function:		createAirport
 * Description:	cTerminates the airport
 * Parameters:	psAirport	- pointer to airport
 *
 * Returned:		none
 *************************************************************************/
void terminateAirport (AirportPtr psAirport)
{
	if (NULL == psAirport)
	{
		fprintf (stderr, "ERROR: NULL AIRPORT PTR\n");
		exit (0);
	}

	pqueueTerminate (&psAirport->sLandingQ);
	queueTerminate (&psAirport->sTakeoffQ);
	createStats (&psAirport->sStats);
}
/**************************************************************************
 * Function:		printHeader
 * Description:	prints the header table
 * Parameters:	none
 *
 * Returned:		none
 *************************************************************************/
void printHeader ()
{
	printf ("     |           Planes Added            |      Runways      |"
                   "    List  Lengths \n");
  printf ("Time | Takeoff  Landing (Fuel Remaining) |  1   2   3  Crash | "
                        "Takeoff  Landing \n");
  printf ("---- | -------  ------------------------ | --- --- --- ----- | "
                        "-------  ------- \n");
}
/**************************************************************************
 * Function:		createAirport
 * Description:	determine if both takeoff and landing queues are empty
 * Parameters:	psAirport	- pointer to airport
 *
 * Returned:		true if both queues are empty; false otherwise
 *************************************************************************/
bool bQueuesEmpty (AirportPtr psAirport)
{
	if (NULL == psAirport)
	{
		fprintf (stderr, "ERROR: NULL AIRPORT PTR\n");
		exit (0);
	}


	return (queueIsEmpty (&psAirport->sTakeoffQ)
			&& pqueueIsEmpty (&psAirport->sLandingQ));
}
/**************************************************************************
 * Function:		enqueueTakeoffPlane
 * Description:	adds a take off plane to the Queue
 * Parameters:	psAirport	- pointer to airport
 *
 * Returned:		none
 *************************************************************************/
void enqueueTakeoffPlane (AirportPtr psAirport)
{
	char Takeoff = 'T';
	if (NULL == psAirport)
	{
		fprintf (stderr, "ERROR: NULL AIRPORT PTR\n");
		exit (0);
	}

	if (!queueIsFull (&psAirport->sTakeoffQ))
	{
		queueEnqueue (&psAirport->sTakeoffQ, &Takeoff, sizeof (char));
		psAirport->sStats.totalTakeOffPlanes++;
	}
}
/**************************************************************************
 * Function:		enqueueLandingPlane
 * Description:	adds a landing plane to the landding queue
 * Parameters:	psAirport	- pointer to airport
 * 							fuel			- fuel remaining in plane
 *
 * Returned:		none
 *************************************************************************/
void enqueueLandingPlane (AirportPtr psAirport, int fuel)
{
	char Landing = 'L';
	if (NULL == psAirport)
	{
		fprintf (stderr, "ERROR: NULL AIRPORT PTR\n");
		exit (0);
	}
	if (!queueIsFull (&psAirport->sTakeoffQ))
	{
		pqueueEnqueue (&psAirport->sLandingQ, &Landing, sizeof (char), fuel);
		psAirport->sStats.totalLandingPlanes++;
	}
}
/**************************************************************************
 * Function:		landPlane
 * Description:	lands a plane from the landing planes Q
 * Parameters:	psAirport	- pointer to airport
 *
 * Returned:		char representing a landing plane; E if 0 fuel remains
 *************************************************************************/
char landPlane (AirportPtr psAirport)
{
	char ch;
	int fuelRemaining;
	if (NULL == psAirport)
	{
		fprintf (stderr, "ERROR: NULL AIRPORT PTR\n");
		exit (0);
	}

	if (!pqueueIsEmpty(&psAirport->sLandingQ))
	{
		pqueueDequeue (&psAirport->sLandingQ, &ch, sizeof (char),&fuelRemaining);
		psAirport->sStats.fuelAtLanding += fuelRemaining;
		if (0 == fuelRemaining)
		{
			ch = 'E';
			psAirport->sStats.numZeroFuelPlanes++;
		}
	}

	return ch;
}
/**************************************************************************
 * Function:		planeTakeoff
 * Description:	remove a plane from takeoff queue
 * Parameters:	psAirport	- pointer to airport
 *
 * Returned:		char representing a takeoff plane;
 *************************************************************************/
char planetakeOff (AirportPtr psAirport)
{
	char ch;
	if (NULL == psAirport)
	{
		fprintf (stderr, "ERROR: NULL AIRPORT PTR\n");
		exit (0);
	}

	if (!queueIsFull (&psAirport->sTakeoffQ))
	{
		queueDequeue (&psAirport->sTakeoffQ, &ch, sizeof (char));
	}

	return ch;
}
/**************************************************************************
 * Function:		assignRunways
 * Description:	assigns a plane to each of the runway positions
 * Parameters:	psAirport	- pointer to airport
 *
 * Returned:		none
 *************************************************************************/
void assignRunways (AirportPtr psAirport)
{
	int i;
	char ch;
	int remainingFuel;
	bool bEmergencyLanding = false;

	if (NULL == psAirport)
	{
		fprintf (stderr, "ERROR: NULL AIRPORT PTR\n");
		exit (0);
	}

	for (i = 0; i < MAX_RUNWAYS; i++)
	{
		if (!bQueuesEmpty (psAirport))
		{
			if (!pqueueIsEmpty (&psAirport->sLandingQ))
			{
				pqueuePeek (&psAirport->sLandingQ, &ch, sizeof (char), &remainingFuel);
				if (0 == remainingFuel)
				{
					bEmergencyLanding = true;
				}
			}
			if (bEmergencyLanding || pqueueSize (&psAirport->sLandingQ) >=
					queueSize (&psAirport->sTakeoffQ))
			{
				ch = landPlane (psAirport);
			}
			else
			{
				ch = planetakeOff (psAirport);
			}

			psAirport->aRunway[i] = ch;
			bEmergencyLanding = false;

		}
	}
}
/**************************************************************************
 * Function:		resetRunway
 * Description:	fills the runway array with '---' to show empty runways
 * Parameters:	psAirport	- pointer to airport
 *
 * Returned:		none
 *************************************************************************/
void resetRunways (AirportPtr psAirport)
{
	char aReset[MAX_RUNWAYS] = "---";
	if (NULL == psAirport)
	{
		fprintf (stderr, "ERROR: NULL AIRPORT PTR\n");
		exit (0);
	}


	strncpy (psAirport->aRunway, aReset, MAX_RUNWAYS);
}
/**************************************************************************
 * Function:		getRunway
 * Description:	copies the runway array to the passed in array
 * Parameters:	psAirport	- pointer to airport
 * 							aRunway		- the runway
 *
 * Returned:		none
 *************************************************************************/
void getRunway (AirportPtr psAirport, char aRunway[])
{
	if (NULL == psAirport)
	{
		fprintf (stderr, "ERROR: NULL AIRPORT PTR\n");
		exit (0);
	}
	if (NULL == aRunway)
	{
		fprintf (stderr, "ERROR: NULL RUNWAY PTR\n");
		exit (0);
	}

	strncpy (aRunway, psAirport->aRunway, MAX_RUNWAYS);
}
/**************************************************************************
 * Function:		landPlane
 * Description:	crashes planes still remaining in air whith zero fuel
 * 							after runway assignment
 * Parameters:	psAirport	- pointer to airport
 *
 * Returned:		int	- number of planes crashed
 *************************************************************************/
int crashPlanes (AirportPtr psAirport)
{
	int crashes = 0, fuelRemaining;
	char ch;
	if (NULL == psAirport)
	{
		fprintf (stderr, "ERROR: NULL AIRPORT PTR\n");
		exit (0);
	}


	do
	{
		if (!pqueueIsEmpty (&psAirport->sLandingQ))
		{
			pqueuePeek (&psAirport->sLandingQ, &ch, sizeof (char), &fuelRemaining);

			if (0 >= fuelRemaining)
			{
				pqueueDequeue (&psAirport->sLandingQ, &ch, sizeof(char), &fuelRemaining);
				crashes++;
			}
		}
		else
		{
			fuelRemaining = -1;
		}
	} while (0 == fuelRemaining);

	psAirport->sStats.numCrashes += crashes;

	return crashes;
}
/**************************************************************************
 * Function:		takeoffQSize
 * Description:	returns the size of the takeoff queue
 * Parameters:	psAirport	- pointer to airport
 *
 * Returned:		int 	- size
 *************************************************************************/
int takeoffQSize(AirportPtr psAirport)
{
	if (NULL == psAirport)
	{
		fprintf (stderr, "ERROR: NULL AIRPORT PTR\n");
		exit (0);
	}

	return queueSize (&psAirport->sTakeoffQ);
}
/**************************************************************************
 * Function:		landingQSize
 * Description:	returns the size of the landing queue
 * Parameters:	psAirport	- pointer to airport
 *
 * Returned:		int	- size
 *************************************************************************/
int landingQSize (AirportPtr psAirport)
{
	if (NULL == psAirport)
	{
		fprintf (stderr, "ERROR: NULL AIRPORT PTR\n");
		exit (0);
	}

	return pqueueSize (&psAirport->sLandingQ);
}
/**************************************************************************
 * Function:		landPlane
 * Description:	reduces the fuel in all landing planes by 1
 * Parameters:	psAirport	- pointer to airport
 *
 * Returned:		none
 *************************************************************************/
void reduceFuel (AirportPtr psAirport)
{
	if (NULL == psAirport)
	{
		fprintf (stderr, "ERROR: NULL AIRPORT PTR\n");
		exit (0);
	}

	if (!pqueueIsEmpty (&psAirport->sLandingQ))
	{
		pqueueChangePriority (&psAirport->sLandingQ, -1);
	}
}
/**************************************************************************
 * Function:		increaseLandingWaitTime
 * Description:	increase total time planes in takeoff q wait
 * Parameters:	psAirport	- pointer to airport
 *
 * Returned:		void
 *************************************************************************/
void increaseLandingWaitTime (AirportPtr psAirport)
{
	if (NULL == psAirport)
	{
		fprintf (stderr, "ERROR: NULL AIRPORT PTR\n");
		exit (0);
	}

	psAirport->sStats.totalLandingWaitTime += landingQSize (psAirport);
}
/**************************************************************************
 * Function:		increaseTakeoffWaitTime
 * Description:	increases the total wait time of takeoff planes
 * Parameters:	psAirport	- pointer to airport
 *
 * Returned:		none
 *************************************************************************/
void increaseTakeoffWaitTime (AirportPtr psAirport)
{
	if (NULL == psAirport)
	{
		fprintf (stderr, "ERROR: NULL AIRPORT PTR\n");
		exit (0);
	}

	psAirport->sStats.totalTakeoffWaitTime += takeoffQSize (psAirport);
}
/**************************************************************************
 * Function:		printStats
 * Description:	prints a summery of the simulation
 * Parameters:	psAirport	- pointer to airport
 *
 * Returned:		none
 *************************************************************************/
void printStats (AirportPtr psAirport)
{
	if (NULL == psAirport)
	{
		fprintf (stderr, "ERROR: NULL AIRPORT PTR\n");
		exit (0);
	}

	printf ("Average takeoff waiting time: %g \n", (double)
			psAirport->sStats.totalTakeoffWaitTime /
					psAirport->sStats.totalTakeOffPlanes);

	printf ("Average landing waiting time: %g \n", (double)
			psAirport->sStats.totalLandingWaitTime /
					psAirport->sStats.totalLandingPlanes);

	printf ("Average flying time remaining: %g \n", (double)
			psAirport->sStats.fuelAtLanding / psAirport->sStats.totalLandingPlanes);

	printf ("Number of planes landing with zero fuel: %d \n",
			psAirport->sStats.numZeroFuelPlanes);

	printf ("Number of crashes: %d \n", psAirport->sStats.numCrashes);
}



