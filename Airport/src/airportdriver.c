/**************************************************************************
 File name:		  airportdriver.c
 Author:        josh napoles
 Date:          11/14/18
 Class:         CS300
 Assignment:    Airport
 Purpose:       Runs the airport simulation
 *************************************************************************/

#include "../include/airport.h"
#include <stdio.h>
/**************************************************************************
 * Function:		printResults
 * Description:	Print the results of a clock tick in table format
 * Parameters:	clock				- represents number of turns
 * 							numTakeoff	- number of takeoff planes added to airport
 * 							numLanding	- number of landing planes added to aiport
 * 							fuel				- array of fuel values for each landing plane
 * 							aRunway			- array of characters that make up the runway
 * 							crashes			- number of crashes this turn
 * 							takeoffQsize- current size of takeoff queue
 * 							landingQsize-	current size of takeoff queue
 * Returned:		none
 *************************************************************************/
void printResults (int clock, int numTakeoff,int numLanding, int fuel[],
		char aRunway[], int crashes, int takeoffQsize, int landingQSize)
{
	int i = 0;
	printf ("%4d |", clock);
	printf ("%8d", numTakeoff);
	printf ("%9d |", numLanding);

	for (i = 0; i < MAX_RUNWAYS; i++)
	{
		if (0 == fuel [i])
		{
			printf ("%5c", '-');
		}
		else
		{
			printf ("%5d", fuel[i]);
		}
	}
	printf (" |");

	for (i = 0; i < MAX_RUNWAYS; i++)
	{
		printf ("%3c ", aRunway[i]);
	}

	printf ("%6d |", crashes);

	printf ("%8d", takeoffQsize);
	printf ("%9d\n", landingQSize);
}
/**************************************************************************
 * Function:		main
 * Description:	run the airport simulation
 * Parameters:	argc		- number of arguements
 * 							argv[1]	-data file to read from
 * Returned:		EXIT_SUCCESS or EXIT_FAILURE
 *************************************************************************/
#define PRINT_HEADER 20
int main (int argc, char **argv)
{
	int clock = 0, i, crashes;
	bool bEOF = false;
	FILE *fptr;
	Airport sA;
	int numLanding, numTakeoff;
	char aRunway [MAX_RUNWAYS];
	int fuel[MAX_RUNWAYS];
	int LQSize, TQSize;

	if (2 != argc)
	{
		fprintf (stderr, "ERROR: Invalid CMD Line options.\n");
		exit (0);
	}
	fptr = fopen (argv[1], "r");
	if (NULL == fptr)
	{
		fprintf (stderr, "Unable to open file %s", argv[1]);
		exit (0);
	}

	createAirport (&sA);

	while (!bEOF || !bQueuesEmpty(&sA))
	{

		if (clock % PRINT_HEADER == 0)
		{
			printHeader ();
		}

		if (EOF != fscanf (fptr, "%d", &numTakeoff))
		{
			fscanf (fptr, "%d %d %d %d", &numLanding, &fuel[0], &fuel[1], &fuel[2]);

			for (i = 0; i < numTakeoff; i++)
			{
				enqueueTakeoffPlane (&sA);
			}

			for (i = 0; i < numLanding; i++)
			{
				enqueueLandingPlane (&sA, fuel[i]);
			}
		}
		else
		{
			bEOF = true;
		}

		clock++;
		increaseLandingWaitTime(&sA);
		increaseTakeoffWaitTime (&sA);
		reduceFuel (&sA);



		assignRunways (&sA);
		getRunway (&sA, aRunway);
		crashes = crashPlanes(&sA);
		TQSize = takeoffQSize (&sA);
		LQSize = landingQSize (&sA);

		printResults (clock, numTakeoff, numLanding, fuel,
			 aRunway, crashes, TQSize, LQSize);

		resetRunways (&sA);
		numTakeoff = 0;
		numLanding = 0;
		crashes = 0;
	}

	printStats (&sA);

	fclose (fptr);
	terminateAirport (&sA);
	return EXIT_SUCCESS;
}
