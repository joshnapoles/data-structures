//******************************************************************************
// File name:		WordSearch.c
// Author:			Josh Napoles
// Date:				9/04/18
// Class:				CS300
// Assignment:	Word Search
// Purpose:			To read a puzzle and a list of words from a file into a 2-D
//              puzzle array and a word struct array respectively. The list of
//              words will be used to search through the puzzle to see if they
//              can be found or not. Orientations include, but are not limited
//              to, horizontal, vertical, diagonal upper right to lower left and
//              diagonal upper left to lower right. The puzzle, words, and
//              whether found (including row & column found & orientation) or
//              not will be outputted.
//
//              A sample file will be
//              2 3              number of rows and columns
//              ABC              row 1 of the puzzle
//              DEF              row 2 of the puzzle
//              AB               word 1
//              ABCD             word 2
//
//							The puzzle will be read into a 2D array that has a border of
//							'\0' (which is a character not in the alphabet) around the
//							puzzle to simplify the search process. The resulting puzzle is
//							shown below, where '@' represents '\0':
//
//              @@@@@
//              @ABC@
//              @DEF@
//              @@@@@
//
//							No bounds checking is necessary because either the word is found
//              such as ABC or for the word ABCD, the D is compared to a '\0'
//              and the search stops.
// Hours:       5
//******************************************************************************
#include <stdio.h>
#include <string.h>
#include <stdbool.h>
#include <stdlib.h>

//******************************************************************************
// GLOBAL CONSTANTS
//******************************************************************************
#define MAX_WORD_LENGTH  32 // includes \0
#define EXTRA_SPACE  2
#define MAX_WORDS  25
#define MAX_ROWS  20
#define MAX_COLS  20
#define PUZZLE_BORDER  1    // 1 element border of '\0' around puzzle
// only 1 is allowed in this implementation

#define MAX_BOARD_COLS  PUZZLE_BORDER + MAX_COLS + PUZZLE_BORDER
#define MAX_BOARD_ROWS  PUZZLE_BORDER + MAX_ROWS + PUZZLE_BORDER

#define MAX_ORIENTATION_DESCRIPTION  20

//******************************************************************************
// PROGRAMMER-DEFINED DATA TYPES
//******************************************************************************

typedef enum DirectionRow
{
	DEC_ROW = -1, SAME_ROW, INC_ROW
} DirectionRow;

typedef enum DirectionCol
{
	DEC_COL = -1, SAME_COL, INC_COL
} DirectionCol;

typedef enum SearchDirection
{
	HORIZONTAL, VERTICAL, DIAGONAL_UL_LR, DIAGONAL_UR_LL, DIAGONAL_LR_UL,
	DIAGONAL_LL_UR
} SearchDirection;

typedef struct OrientationData
{
	DirectionRow eRowDirection;
	DirectionCol eColumnDirection;
	SearchDirection eSearchDirection;
	char szDirectionDesription[MAX_ORIENTATION_DESCRIPTION];
} OrientationData;

static OrientationData gOrientationData[] = { { SAME_ROW, INC_COL, HORIZONTAL,
		"Horizontally" }, { INC_ROW, SAME_COL, VERTICAL, "Vertically" }, { INC_ROW,
		INC_COL, DIAGONAL_UL_LR, "Diagonally UL to LR" }, { INC_ROW, DEC_COL,
		DIAGONAL_UR_LL, "Diagonally UR to LL" }, {DEC_ROW, DEC_COL,
				DIAGONAL_LR_UL, "Diagonal LR to UL"} ,{DEC_ROW, INC_COL,
						DIAGONAL_LL_UR, "Diagonal LL to UR"} };

typedef struct Word
{
	char szWord[MAX_WORD_LENGTH];
	int rowFound;
	int colFound;
	bool bIsFound;
	SearchDirection eOrientation;
} Word;

//******************************************************************************
// FUNCTION PROTOTYPES
//******************************************************************************
void addBorder (char aPuzzle[][MAX_BOARD_COLS], int numRows, int numCols,
		char border);

void readPuzzle (FILE *pInFile, char aPuzzle[][MAX_BOARD_COLS], int *pNumRows,
		int *pNumCols, char border);

void printHeading (int numRows, int numCols);

void printPuzzle (char aPuzzle[][MAX_BOARD_COLS], int numRows, int numCols);

void readWords (FILE *pInFile, Word asWordList[], int *pNumWords);

void printWords (const Word asWordList[], int numWords);

void setFoundPosition (Word asWordList[], int whichWord, int row, int column,
		SearchDirection eOrientation);

void findAllWords (char aPuzzle[][MAX_BOARD_COLS], Word asWordList[],
		int numRows, int numCols, int numWords,
		const SearchDirection aeSearchDirections[], int searchDirectionsSize);

bool wordSearch (char puzzle[][MAX_BOARD_COLS], const char word[], int row,
		int col, DirectionRow rowDirection, DirectionCol columnDirection);

//******************************************************************************
//Function:			main
//Description:	To load a puzzle and word list into appropriate variables
//							so that a word search can be performed on each word
//							throughout the puzzle. This program is written generically
//							enough so that the search directions can be loaded into
//						  an array along with the number of searches to perform
//							and the results of the various searches throughout the
//							puzzle on all words will be displayed.
//
//Parameters:		none
//
//Returned:			EXIT_SUCCESS
//******************************************************************************
int main ()
{
	const char PUZZLE_NAME[] = "data/puzzle.txt";
	const char BORDER = '\0';

	char aPuzzle[MAX_BOARD_ROWS][MAX_BOARD_COLS];
	Word asWordList[MAX_WORDS];
	FILE *pInFile;
	int numRows;
	int numCols;
	int numWords = 0;

	// Search directions are loaded up in an array along with the number of
	// searches to be performed. These two variables are passed to findAllWords.
	// In this case we are searching horizontally, vertically, and diagonally
	// upper-left to lower-right and upper-right to lower-left. The 4 is used
	// as the total number of searches in the array searchDirections.
	SearchDirection searchDirections[] = { HORIZONTAL, VERTICAL, DIAGONAL_UL_LR,
			DIAGONAL_UR_LL, DIAGONAL_LR_UL, DIAGONAL_LL_UR};
	const int SEARCH_DIRECTIONS_SIZE = 6;

	pInFile = fopen (PUZZLE_NAME, "r");

	if (NULL == pInFile)
	{
		puts ("Error opening file\n");
		exit (EXIT_FAILURE);
	}

	readPuzzle (pInFile, aPuzzle, &numRows, &numCols, BORDER);

	printHeading (numRows, numCols);

	printPuzzle (aPuzzle, numRows, numCols);

	readWords (pInFile, asWordList, &numWords);

	findAllWords (aPuzzle, asWordList, numRows, numCols, numWords,
			searchDirections, SEARCH_DIRECTIONS_SIZE);

	printWords (asWordList, numWords);

	fclose (pInFile);

	return EXIT_SUCCESS;
}

//******************************************************************************
//Function:	 	 addBorder
//
//Description: Initializes each element of the puzzle's border to the given
//						 character which cannot be part of a word. This border removes the
//						 need for bounds checking in the 2D array since each search will
//						 terminate at the border when the letter from the word fails to
//						 match the border.
//
//Parameters:  aPuzzle	- 2-D array of characters where the data from the file
//												is being stored
//						 pNumRows	- number of rows in the puzzle
//						 pNumCols	-	number of columns in the puzzle
//						 border	  - the character to use to fill the border
//
//Returned:    none
//******************************************************************************
void addBorder (char aPuzzle[][MAX_BOARD_COLS], int numRows, int numCols,
		char border)
{
	int i, j;
	char aTempPuzzle[numRows][numCols];

	//copy aPuzzle to aTempPuzzle
	for (i = 0; i < numRows; i++)
	{
		for (j = 0; j < numCols; j++)
		{
			aTempPuzzle[i][j] = aPuzzle[i][j];
		}
	}


	numRows+=EXTRA_SPACE;
	numCols+=EXTRA_SPACE;

 //fill aPuzzle to include a border of \0
	for (i = 0; i < numRows; i++)
	{
		for (j = 0; j < numCols; j++)
		{
			if ((0 == i || 0 == j) || (i == numRows - 1 || j == numCols - 1))
			{
				aPuzzle [i][j] = '\0';
			}
			else
			{
				aPuzzle [i][j] = aTempPuzzle [i-1][j-1];
			}

		}
	}
}

//******************************************************************************
//Function:	 	 readPuzzle
//
//Description: To read the puzzle from a file into a 2-D character
//						 array. The puzzle is read starting at position [1][1]
//						 because a border is placed around the puzzle for efficient
//						 searching.  This function invokes the addBorder() function to
//						 place the border around the puzzle.
//
//Parameters:  pInFile		- file buffer variable
//						 aPuzzle	  - 2-D array of characters where the data from the file
//												  is being stored
//						 pNumRows	  - number of rows in the puzzle
//						 pNumCols	  -	number of columns in the puzzle
//						 border     - the character to use to fill the border
//
//Returned:    none
//******************************************************************************
void readPuzzle (FILE *pInFile, char aPuzzle[][MAX_BOARD_COLS], int *pNumRows,
		int *pNumCols, char border)
{
	int row, col;

	fscanf (pInFile, "%d %d", pNumRows, pNumCols);

	if (*pNumRows > MAX_ROWS || *pNumCols > MAX_COLS)
	{
		puts ("Maximum rows or columns in datafile exceeded\n");
		getchar ();
		exit (EXIT_FAILURE);
	}

	addBorder (aPuzzle, *pNumRows, *pNumCols, border);

	for (row = 1; row <= *pNumRows; ++row)
	{
		for (col = 1; col <= *pNumCols; ++col)
		{
			fscanf (pInFile, " %c", &aPuzzle[row][col]);
		}
	}
}

//******************************************************************************
//Function:	 	 printHeading
//
//Description: Prints the heading and rows & columns read from the file
//
//Parameters:  numRows	- number of rows in the puzzle
//						 numCols	-	number of columns in the puzzle
//
//Returned:    none
//******************************************************************************
void printHeading (int numRows, int numCols)
{
	puts ("******************************");
	puts ("*        Find A Word         *");
	puts ("******************************\n");

	printf ("Number of Rows:     %3d\n", numRows);
	printf ("Number of Columns:  %3d\n\n", numCols);

	puts ("Puzzle\n------\n");
}

//******************************************************************************
//Function:			printPuzzle
//
//Description:	Prints the puzzle to the screen. The border is not printed.
//
//Parameters:		aPuzzle	- 2-D array holding the puzzle
//							numRows	- number of rows in the puzzle
//							numCols	-	number of columns in the puzzle
//
//Returned:     none
//******************************************************************************
void printPuzzle (char aPuzzle[][MAX_BOARD_COLS], int numRows, int numCols)
{
	int row, col;

	for (row = 1; row <= numRows; ++row)
	{
		for (col = 1; col <= numCols; ++col)
		{
			printf ("%c", aPuzzle[row][col]);
		}
		puts ("");
	}
}

//******************************************************************************
//Function:			readWords
//
//Description:	Reads the words into an array of structs
//
//Parameters:		pInfile	   - file buffer variable
//							asWordList - array of structs containing the word, and
//												   information on whether the word is found or not.
//                           A found word includes starting row and column
//                           found along with the orientation (e.g. horizontal)
//                           of how the word is found
//							numWords   - number of words in the list
//
//Returned:     none
//******************************************************************************

void readWords (FILE *pInFile, Word asWordList[], int *pNumWords)
{
	char szWord[MAX_WORD_LENGTH];
	int wordCount;

	for (wordCount = 0; wordCount < MAX_WORDS && !feof (pInFile); ++wordCount)
	{
		fscanf (pInFile, "%s", szWord);

		// adding 1 for null termination
		strncpy (asWordList[wordCount].szWord, szWord, strlen (szWord) + 1);
		asWordList[wordCount].bIsFound = false;
		++*pNumWords;
	}

	// if words in datafile exceeds MAX_WORDS, the remaining words will not
	// be read in
}

//******************************************************************************
//Function:			printWords
//
//Description:	Prints the information from the word list to the screen
//
//Parameters:		asWordList - array of structs containing the information on
//												   the words
//							numWords   - the number of words in the list
//
//Returned:     none
//******************************************************************************
void printWords (const Word asWordList[], int numWords)
{
	int whichWord;

	puts ("\nWords\n-----\n");

	for (whichWord = 0; whichWord < numWords; ++whichWord)
	{
		printf ("%-32s", asWordList[whichWord].szWord);

		if (asWordList[whichWord].bIsFound)
		{
			printf ("FOUND (Row: %d  Column: %d %s\n", asWordList[whichWord].rowFound,
					asWordList[whichWord].colFound,
					gOrientationData[asWordList[whichWord].eOrientation].szDirectionDesription);
		}
		else
		{
			puts ("NOT FOUND");
		}
	}
}

//******************************************************************************
//Function:	 	 setFoundPosition
//
//Description: The word is found so set row & column found along with the
//						 the orientation
//
//Parameters:  asWordList		 - the words to be searched for
//						 whichWord     - the particular word being searched for
//						 row			     - current row found
//						 column        - current column found
//             eOrientation  - the orientation of the found word
//
//Returned:    none
//******************************************************************************
void setFoundPosition (Word asWordList[], int whichWord, int row, int column,
		SearchDirection eOrientation)
{
	asWordList[whichWord].rowFound = row;
	asWordList[whichWord].colFound = column;
	asWordList[whichWord].eOrientation = eOrientation;
}

//******************************************************************************
//Function:			findAllWords
//
//Description:	This function takes the puzzle and an array of words.  Each word
//							in the array is searched for in the puzzle.  An array of
//							SearchDirections is also provided to define in what directions
//							words should be searched for.
//
//							If the first letter in the word matches a puzzle
//              letter, a function is called, per direction in the direction
//							array, to find if the whole word is matched. If so, all matching
//							information is stored.  The function stops searching for a
//							word once found and only returns the first position
//							and direction that matches.
//
//Parameters:		aPuzzle	               - 2-D array of characters holding the
//                                       puzzle
//							asWordList             - array holding the words and other
//												               information  concerning the word
//							numRows	               - the number of rows in the puzzle
//							numCols	               - the number of columns in the puzzle
//							numWords               - the number of words in the wordlist
//							aeSearchDirections     - array of directions used for searching
//							searchDirectionsSize   - size of the searchDirections array
//
//Returned:     none
//******************************************************************************
void findAllWords (char aPuzzle[][MAX_BOARD_COLS], Word asWordList[],
		int numRows, int numCols, int numWords,
		const SearchDirection aeSearchDirections[], int searchDirectionsSize)
{
	SearchDirection eDirection;
	int row, col;
	int whichWord;
	int dirIndex;

	for (row = 1; row <= numRows; ++row)
	{
		for (col = 1; col <= numCols; ++col)
		{
			for (whichWord = 0; whichWord < numWords; ++whichWord)
			{
				if (!asWordList[whichWord].bIsFound                                //word is not found AND
						&& aPuzzle[row][col] == asWordList[whichWord].szWord[0])       //first letter is the same
				{
					for (dirIndex = 0;
							!asWordList[whichWord].bIsFound && dirIndex < searchDirectionsSize; //word is not found AND we have directions left to search
							dirIndex++)
					{
						eDirection = aeSearchDirections[dirIndex];               // set which direction to search

						asWordList[whichWord].bIsFound = wordSearch (aPuzzle,     //returns if word is found or not
								asWordList[whichWord].szWord, row, col,
								gOrientationData[eDirection].eRowDirection,
								gOrientationData[eDirection].eColumnDirection);

						if (asWordList[whichWord].bIsFound)
						{
							setFoundPosition (asWordList, whichWord, row, col, eDirection);
						}
					}
				}
			}
		}
	}
}

//******************************************************************************
//Function:	 	 wordSearch
//
//Description: To determine if a word exists in the puzzle in a particular
//             direction at a particular starting location
//
//Parameters:  aPuzzle		      - 2-D puzzle array
//						 szWord			      - array of characters containing the letters of
//												        the word being searched for
//						 row			        - row in the puzzle where the search starts
//						 col			        - column in the puzzle where the search starts
//						 eRowDirection    - row increment while searching for the word
//						 eColumnDirection - column increment while searching for the word
//
//Returned:    true if word found; otherwise, false
//******************************************************************************
bool wordSearch (char aPuzzle[][MAX_BOARD_COLS], const char szWord[], int row,
		int col, DirectionRow eRowDirection, DirectionCol eColumnDirection)
{
	int i = row,j = col;
	int whichLetter = 0;
	int wordLength = 0;
	bool bWordIsFound = false;

	//find length of word
	while (szWord [wordLength] != '\0')
	{
		wordLength++;
	}


	while (aPuzzle [i][j] == szWord [whichLetter] && !bWordIsFound)
	{

		if (whichLetter+1 == wordLength)
		{
			bWordIsFound = true;
		}

		i += eRowDirection;
		j += eColumnDirection;
		whichLetter++;

	}


	return bWordIsFound;
}
