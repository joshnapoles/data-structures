/**************************************************************************
 File name:  stk.c
 Author:     Josh Napoles
 Date:			 9.24.18
 Class:			 CS300
 Assignment: Generic Static Stack
 Purpose:    Interface for a static stack of generic elements
 *************************************************************************/
#include "../include/stk.h"
#include <stdio.h>
#include <stdlib.h>

#define MAX_ERROR_CHARS 64
char gszErrors [STK_NUMBER_OF_ERRORS][MAX_ERROR_CHARS];

/**************************************************************************
 * Function:		stkLoadErrorMessages
 *
 * Description:	load the stack error messages
 *
 * Parameters:	none
 *
 * Returned:		none
 *************************************************************************/
void stkLoadErrorMessages ()
{
	LOAD_ERRORS
}
/**************************************************************************
 * Function:		stkCreate
 *
 * Description:	create a stack
 *
 * Parameters:	psStack	-	pointer to stack that is to be created
 *
 * Returned:		none
 *************************************************************************/
void stkCreate (StackPtr psStack)
{
	if (NULL == psStack || NULL == psStack->pData[0])
	{
		fprintf (stderr, "%s\n", gszErrors [STK_NO_CREATE_ERROR]);
		exit(0);
	}
	else
	{
		psStack->top = -1;
		psStack->pData[0] = NULL;
	}
}
/**************************************************************************
 * Function:		stkIsFull
 *
 * Description:	checks to see if the stack is full
 *
 * Parameters:	psStack	-	pointer to stack that is to be checked
 *
 * Returned:		bool	- true if stack is full false otherwise
 *************************************************************************/
bool stkIsFull (const StackPtr psStack)
{
	bool bIsFull = false;

	if (NULL == psStack)
	{
		fprintf (stderr, "%s\n", gszErrors [STK_NO_CREATE_ERROR]);
		exit(0);
	}

	if (STK_MAX_ELEMENTS <= stkSize(psStack))
	{
		bIsFull = true;
	}

	return bIsFull;
}
/**************************************************************************
 * Function:		stkIsEmpty
 *
 * Description:	checks to see if the stack is empty
 *
 * Parameters:	psStack	-	pointer to stack that is to be checked
 *
 * Returned:		bool	- true if stack is empty; false otherwise
 *************************************************************************/
bool stkIsEmpty (const StackPtr psStack)
{
	bool bIsEmpty = false;

	if (NULL == psStack)
	{
		fprintf (stderr, "%s\n", gszErrors [STK_NO_CREATE_ERROR]);
		exit(0);
	}

	if (0 == stkSize (psStack))
	{
		bIsEmpty = true;
	}

	return bIsEmpty;
}
/**************************************************************************
 * Function:		stkSize
 *
 * Description:	returns the number of elements in the stack
 *
 * Parameters:	psStack	-	pointer to stack that is to be checked
 *
 * Returned:		int 		-	size of stack
 *************************************************************************/
int stkSize (const StackPtr psStack)
{
	if (NULL == psStack)
	{
		fprintf (stderr, "%s\n", gszErrors [STK_NO_CREATE_ERROR]);
		exit(0);
	}

	int topOfArray = psStack->top;

	return topOfArray + 1;
}
/**************************************************************************
 * Function:		stkPush
 *
 * Description:	Pushes an element to the top of the stack
 *
 * Parameters:	psStack	-	the stack
 * 							pBuffer	-	the element that will be pushed on the stack
 * 							size		-	the size of the element
 *
 * Returned:		none
 *************************************************************************/
void stkPush (StackPtr psStack, void *pBuffer, int size)
{
	if (NULL == psStack)
	{
		fprintf (stderr, "%s\n", gszErrors [STK_NO_CREATE_ERROR]);
		exit(0);
	}

	if (!stkIsFull(psStack))
	{
		psStack->top++;

		psStack->pData[psStack->top] = malloc (size);
		memcpy (psStack->pData[psStack->top], pBuffer, size);
	}
	else
	{
		fprintf (stderr, "%s\n", gszErrors[STK_FULL_ERROR]);
		exit (0);
	}

}
/**************************************************************************
 * Function:		stkPop
 *
 * Description:	Pops an element from the top of the stack
 *
 * Parameters:	psStack	-	the stack
 * 							pBuffer	-	element that is popped off the stack
 * 							size		- size of the popped off element
 *
 * Returned:		void* - pointer the the popped off element
 *************************************************************************/
void *stkPop (StackPtr psStack, void *pBuffer, int size)
{
	if (NULL == psStack)
	{
		fprintf (stderr, "%s\n", gszErrors [STK_NO_CREATE_ERROR]);
		exit(0);
	}


	if (!stkIsEmpty(psStack))
	{
		memcpy (pBuffer, psStack->pData[psStack->top], size);
		free (psStack->pData[psStack->top]);
		psStack->top--;
	}
	else
	{
		fprintf (stderr, "%s\n", gszErrors[STK_EMPTY_ERROR]);
		exit (0);
	}


	return pBuffer;
}
/**************************************************************************
 * Function:		stkTerminate
 *
 * Description:	terminates the stack
 *
 * Parameters:	psStack	-	the stack to be terminated
 *
 * Returned:		none
 *************************************************************************/
void stkTerminate (StackPtr psStack)
{
	int i;
	int stkHeight = stkSize (psStack);

	if (NULL == psStack)
	{
		fprintf (stderr, "%s\n", gszErrors [STK_NO_CREATE_ERROR]);
		exit(0);
	}

	for (i = stkHeight; i > 0; i--)
	{
		free (psStack->pData[i-1]);
		psStack->top--;
	}
}
/**************************************************************************
 * Function:		stkPeek
 *
 * Description:	Peeks at the top of the stack
 *
 * Parameters:	psStack	-	pointer to stack that is to be checked
 * 							pBuffer	-	pointer to the top element of the stack
 *
 *
 * Returned:		void*		-	pointer to the top element of the stack
 *************************************************************************/
void *stkPeek (const StackPtr psStack, void *pBuffer, int size)
{
	if (NULL == psStack)
	{
		fprintf (stderr, "%s\n", gszErrors [STK_NO_CREATE_ERROR]);
		exit(0);
	}

	if (!stkIsEmpty(psStack))
	{
		memcpy (pBuffer, psStack->pData[psStack->top], size);
	}
	else
	{
		fprintf (stderr, "%s\n", gszErrors[STK_EMPTY_ERROR]);
		exit (0);
	}

	return pBuffer;
}
