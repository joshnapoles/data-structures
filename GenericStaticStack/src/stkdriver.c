/**************************************************************************
 File name:  stkdriver.c
 Author:     Josh Napoles
 Date:			 9.24.18
 Class:			 CS300
 Assignment: Generic Static Stack
 Purpose:    Interface for a static stack of generic elements
 *************************************************************************/

#include <stdio.h>
#include <stdlib.h>

#include "../include/stk.h"

/**************************************************************************
 * Function:		success
 *
 * Description:	print a success message
 *
 * Parameters:	pszStr	-	the message to print
 *
 * Returned:		none
 *************************************************************************/
static void success (char *pszStr)
{
	printf ("SUCCESS: %s\n", pszStr);
}
/**************************************************************************
 * Function:		failure
 *
 * Description:	print a failure message
 *
 * Parameters:	pszStr	-	the message to print
 *
 * Returned:		none
 *************************************************************************/
static void failure (char *pszStr)
{
	printf ("FAILURE: %s\n", pszStr);
}
/**************************************************************************
 * Function:		assert
 *
 * Description:	if the expression is true, assert success; otherwise,
 * 							assert failure
 *
 * Parameters:	bExpression  - true or false expression to be asserted
 * 							pszTrue		   - the message to print when true
 * 							pszFalse	   - the message to print when false
 *
 * Returned:		none
 *************************************************************************/
static void assert (bool bExpression, char *pszTrue, char *pszFalse)
{
	if (bExpression)
	{
		success (pszTrue);
	}
	else if (!bExpression)
	{
		failure (pszFalse);
	}
}
/**************************************************************************
 * Function:		main
 *
 * Description:	test all the functionality of the stack
 *
 * Parameters:	none
 *
 * Returned:		Exit Status
 *************************************************************************/
int main ()
{
	Stack sTheStack;
	Stack sCharStack;
	char letter = 'A';
	int i, value;

	puts ("Program Start\n");

	puts ("SUCCESS TESTS:\n");
	stkLoadErrorMessages();
	success("Loaded Error Messages");

	stkCreate(&sTheStack);
	assert (-1 == sTheStack.top && NULL == sTheStack.pData[0],
			"Stack Created", "Stack Not Created");

	assert (0 == stkSize (&sTheStack),
			"Stack Size is 0", "Stack Size is not 0");

	assert (stkIsEmpty(&sTheStack), "Stack is empty", "Stack is not empty");

	assert (!stkIsFull(&sTheStack), "Stack is not full", "Stack is full");

	// if there is one item in the stack, top points to index 0 in array, but
	// the size of the stack is 1

	printf("\nPushing 10 elements on the Stack\n");
	for (i = 1; i <= 10; i++)
	{
		stkPush (&sTheStack, &i, sizeof (int));
	}
	assert (10 == stkSize (&sTheStack),
			"Stack Size is 10\n", "Stack Size is not 10\n");

	printf ("Peeking into the stack\n");
	assert (value == *(int *) stkPeek
			(&sTheStack, &value, sizeof (int)),
			"Top Element is 10", "Top Element not 10");

	printf("Popping 5 elements off the stack\n");
	for (i = 1; i <= 5; i++)
	{
		printf("Popped Value: %d\n",
				*(int *) stkPop (&sTheStack, &value, sizeof (int)));
	}
	assert (5 == stkSize(&sTheStack),
			"5 Elements on Stack", "5 Elements not on stack");


	//////////////////
	stkCreate (&sCharStack);
	printf("\nPushing 10 elements on the Stack\n");
	for (i = 1; i <= 10; i++)
	{
		stkPush (&sCharStack, &letter, sizeof (char));
		letter++;
	}
	assert (10 == stkSize (&sCharStack),
			"Stack Size is 10\n", "Stack Size is not 10\n");

	printf ("Peeking into the stack\n");
	assert (letter == *(char *) stkPeek
			(&sCharStack, &letter, sizeof (char)),
			"Top Element is J", "Top Element not J");

	printf("Popping 5 elements off the stack\n");
	for (i = 1; i <= 5; i++)
	{
		printf("Popped Value: %c\n",
				*(char *) stkPop (&sCharStack, &letter, sizeof (char)));
	}
	assert (5 == stkSize(&sCharStack),
			"5 Elements on Stack", "5 Elements not on stack");

	stkTerminate (&sCharStack);

	////////////////////////////


	printf ("Terminating the stack\n");
	stkTerminate(&sTheStack);

	assert (stkIsEmpty(&sTheStack), "Stack is empty", "Stack is not empty");

	puts ("\nProgram End");
	return EXIT_SUCCESS;
}

